webpackJsonp([12],{

/***/ 868:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _semanticUiReact = __webpack_require__(126);

var _Diag = __webpack_require__(406);

var _Diag2 = _interopRequireDefault(_Diag);

var _store = __webpack_require__(404);

var _store2 = _interopRequireDefault(_store);

var _helper = __webpack_require__(405);

var _helper2 = _interopRequireDefault(_helper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var btnStyle = {
    marginLeft: '10px'
};

var labelStyle = {
    marginLeft: '3.5em',
    width: '160%',
    lineHeight: '24px',
    fontSize: '12px'
};

var selectStyle = {
    width: '31%',
    marginLeft: '.5em',
    marginRight: '.5em'
};

var TicketCard = function (_Component) {
    _inherits(TicketCard, _Component);

    function TicketCard() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, TicketCard);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = TicketCard.__proto__ || Object.getPrototypeOf(TicketCard)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
            ticket: null,
            clients: [],
            admins: [],
            ticket_statuses: [],
            diag: {
                open: false
            }
        }, _this.componentDidMount = function () {
            var _store$getState = _store2.default.getState(),
                selectedTicketState = _store$getState.selectedTicketState,
                userState = _store$getState.userState;

            if (_this.props.isNew) {
                _this.setState({
                    ticket: {
                        title: '',
                        description: '',
                        client_id: '',
                        ticket_status_id: '',
                        ticket_status: {
                            name: 'Новая'
                        }
                    }
                });
            } else {
                if (selectedTicketState != null) {
                    _this.setState({
                        ticket: selectedTicketState
                    });
                } else {
                    _this.getTicket();
                }
            }

            if (userState.isAdmin) {
                _this.getClientsAndAdmins();
                _this.getTicketStatuses();
            }
        }, _this.getTicketStatuses = function () {
            _helper2.default.http_send({
                url: 'admin/tickets',
                method: 'getTicketStatuses',
                onSuccess: function onSuccess(data) {
                    var ticket_statuses = [];
                    data.items.forEach(function (e) {
                        ticket_statuses.push({
                            key: e.id,
                            value: e.id,
                            text: e.name
                        });
                    });
                    _this.setState({
                        ticket_statuses: ticket_statuses
                    });
                }
            });
        }, _this.getClientsAndAdmins = function () {
            _helper2.default.http_send({
                url: 'admin/users',
                method: 'getClientsAndAdmins',
                onSuccess: function onSuccess(data) {
                    var clients = [];
                    var admins = [];
                    data.clients.forEach(function (e) {
                        clients.push({
                            key: e.id,
                            value: e.id,
                            text: e.user_info.name_f + ' ' + e.user_info.name_i + ' ' + e.user_info.name_o
                        });
                    });
                    data.admins.forEach(function (e) {
                        admins.push({
                            key: e.id,
                            value: e.id,
                            text: e.email
                        });
                    });
                    _this.setState({
                        clients: clients,
                        admins: admins
                    });
                }
            });
        }, _this.getTicket = function () {
            _helper2.default.http_send({
                url: 'admin/tickets',
                method: 'getById',
                send: {
                    id: _this.props.ticketId
                },
                onSuccess: function onSuccess(data) {
                    _this.setState({
                        ticket: data.item
                    });
                }
            });
        }, _this.handleTicketChange = function (e, _ref2) {
            var value = _ref2.value,
                field_type = _ref2.field_type;


            var ticket = _extends({}, _this.state.ticket);
            ticket[field_type] = value;

            _this.setState({
                ticket: ticket
            });
        }, _this.handleSave = function () {
            var ticket = _this.state.ticket;


            _this.ticketValidate(function () {
                _helper2.default.http_send({
                    url: 'admin/tickets',
                    method: 'saveTicket',
                    send: {
                        ticket: ticket
                    },
                    onSuccess: function onSuccess(data) {
                        var ticket = _extends({}, _this.state.ticket);

                        _this.setState({
                            ticket: ticket
                        });
                    }
                });
            });
        }, _this.handleDelete = function () {
            _this.setState({
                diag: {
                    open: true,
                    size: 'mini',
                    title: 'Удаление задачи',
                    text: 'Вы уверены что хотите удалить эту задачу?',
                    onClose: function onClose() {
                        _this.setState({
                            diag: {
                                open: false
                            }
                        });
                    },
                    btns: [{
                        title: 'Удалить',
                        color: 'red',
                        onClick: function onClick() {
                            _helper2.default.http_send({
                                url: 'admin/tickets',
                                method: 'delete',
                                send: {
                                    id: _this.props.ticketId
                                },
                                onSuccess: function onSuccess(data) {
                                    _this.state.diag.onClose();
                                    _helper2.default.goToPage('tickets');
                                }
                            });
                        }
                    }, {
                        title: 'Отмена',
                        color: 'blue',
                        onClick: function onClick() {
                            _this.state.diag.onClose();
                        }
                    }]
                }
            });
        }, _this.handleCreate = function () {
            var ticket = _this.state.ticket;


            _this.ticketValidate(function () {
                _helper2.default.http_send({
                    url: 'admin/tickets',
                    method: 'createTicket',
                    send: {
                        ticket: ticket
                    },
                    onSuccess: function onSuccess(data) {
                        _helper2.default.goToPage('tickets');
                    }
                });
            });
        }, _this.handleCancel = function () {
            _helper2.default.goToPage('tickets');
        }, _this.ticketValidate = function () {
            var nextFunction = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {};
            var ticket = _this.state.ticket;

            var _store$getState2 = _store2.default.getState(),
                userState = _store$getState2.userState;

            var s = '';
            if (ticket.title.length < 3) {
                s = '[Слишком короткий заголовок]';
            }
            if (userState.isAdmin && !ticket.client_id > 0) {
                if (s != '') {
                    s += ', ';
                }
                s += '[Клиент не выбран]';
            }
            if (userState.isAdmin && !ticket.ticket_status_id > 0) {
                if (s != '') {
                    s += ', ';
                }
                s += '[Статус не выбран]';
            }
            if (s == '') {
                nextFunction();
            } else {
                _this.setState({
                    diag: {
                        open: true,
                        title: 'Ошибка создания',
                        text: s,
                        onClose: function onClose() {
                            _this.setState({
                                diag: {
                                    open: false
                                }
                            });
                        },
                        btns: [{
                            title: 'Понятно',
                            color: 'green',
                            onClick: function onClick() {
                                _this.state.diag.onClose();
                            }
                        }]
                    }
                });
            }
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(TicketCard, [{
        key: 'render',
        value: function render() {
            var isNew = this.props.isNew;
            var _state = this.state,
                ticket = _state.ticket,
                clients = _state.clients,
                admins = _state.admins,
                ticket_statuses = _state.ticket_statuses;

            var _store$getState3 = _store2.default.getState(),
                userState = _store$getState3.userState;

            if (ticket != null && ticket.description == null) {
                ticket.description = '';
            }

            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                    _semanticUiReact.Header,
                    { size: 'medium' },
                    '\u041A\u0430\u0440\u0442\u043E\u0447\u043A\u0430 \u0437\u0430\u0434\u0430\u0447\u0438'
                ),
                _react2.default.createElement(
                    'div',
                    null,
                    ticket == null ? _react2.default.createElement(
                        'p',
                        null,
                        '\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430...'
                    ) : userState.isAdmin ? _react2.default.createElement(
                        _semanticUiReact.Form,
                        null,
                        _react2.default.createElement(
                            _semanticUiReact.Form.Group,
                            null,
                            _react2.default.createElement(_semanticUiReact.Form.Input, { field_type: 'title', placeholder: '\u0417\u0430\u0433\u043E\u043B\u043E\u0432\u043E\u043A', value: ticket.title, width: 6, onChange: this.handleTicketChange }),
                            _react2.default.createElement(_semanticUiReact.Form.Field, { field_type: 'client_id', control: _semanticUiReact.Select, placeholder: '\u041A\u043B\u0438\u0435\u043D\u0442', options: clients, value: ticket.client_id, width: 6, onChange: this.handleTicketChange }),
                            _react2.default.createElement(_semanticUiReact.Form.Field, { field_type: 'ticket_status_id', control: _semanticUiReact.Select, placeholder: '\u0421\u0442\u0430\u0442\u0443\u0441', options: ticket_statuses, value: ticket.ticket_status_id, width: 6, onChange: this.handleTicketChange })
                        ),
                        _react2.default.createElement(
                            _semanticUiReact.Form.Group,
                            null,
                            _react2.default.createElement(_semanticUiReact.Form.Field, { field_type: 'description', control: _semanticUiReact.TextArea, placeholder: '\u041E\u043F\u0438\u0441\u0430\u043D\u0438\u0435', value: ticket.description, width: 16, onChange: this.handleTicketChange })
                        )
                    ) : isNew ? _react2.default.createElement(
                        _semanticUiReact.Form,
                        null,
                        _react2.default.createElement(
                            _semanticUiReact.Form.Group,
                            null,
                            _react2.default.createElement(_semanticUiReact.Form.Input, { field_type: 'title', placeholder: '\u0417\u0430\u0433\u043E\u043B\u043E\u0432\u043E\u043A', value: ticket.title, width: 6, onChange: this.handleTicketChange }),
                            _react2.default.createElement(
                                _semanticUiReact.Label,
                                { style: labelStyle, tag: true },
                                ticket.ticket_status.name
                            )
                        ),
                        _react2.default.createElement(
                            _semanticUiReact.Form.Group,
                            null,
                            _react2.default.createElement(_semanticUiReact.Form.Field, { field_type: 'description', control: _semanticUiReact.TextArea, placeholder: '\u041E\u043F\u0438\u0441\u0430\u043D\u0438\u0435', value: ticket.description, width: 16, onChange: this.handleTicketChange })
                        )
                    ) : _react2.default.createElement(
                        _semanticUiReact.Form,
                        null,
                        _react2.default.createElement(
                            _semanticUiReact.Form.Group,
                            null,
                            _react2.default.createElement(
                                _semanticUiReact.Label,
                                { style: labelStyle },
                                ticket.title
                            ),
                            _react2.default.createElement(
                                _semanticUiReact.Label,
                                { style: labelStyle, tag: true },
                                ticket.ticket_status.name
                            )
                        ),
                        _react2.default.createElement(
                            _semanticUiReact.Form.Group,
                            null,
                            _react2.default.createElement(
                                _semanticUiReact.Label,
                                { style: labelStyle },
                                _react2.default.createElement(
                                    'p',
                                    null,
                                    ticket.description
                                )
                            )
                        )
                    )
                ),
                _react2.default.createElement(
                    'div',
                    null,
                    isNew ? _react2.default.createElement(
                        'span',
                        null,
                        _react2.default.createElement(
                            _semanticUiReact.Button,
                            { basic: true, color: 'green', onClick: this.handleCreate },
                            '\u0421\u043E\u0437\u0434\u0430\u0442\u044C'
                        ),
                        _react2.default.createElement(
                            _semanticUiReact.Button,
                            { style: btnStyle, basic: true, color: 'blue', onClick: this.handleCancel },
                            '\u041E\u0442\u043C\u0435\u043D\u0430'
                        )
                    ) : _react2.default.createElement(
                        'span',
                        null,
                        userState.isAdmin == false || _react2.default.createElement(
                            'span',
                            null,
                            _react2.default.createElement(
                                _semanticUiReact.Button,
                                { basic: true, color: 'green', onClick: this.handleSave },
                                '\u0421\u043E\u0445\u0440\u0430\u043D\u0438\u0442\u044C'
                            ),
                            _react2.default.createElement(
                                _semanticUiReact.Button,
                                { style: btnStyle, basic: true, color: 'red', onClick: this.handleDelete },
                                '\u0423\u0434\u0430\u043B\u0438\u0442\u044C'
                            )
                        ),
                        _react2.default.createElement(
                            _semanticUiReact.Button,
                            { style: btnStyle, basic: true, color: 'blue', onClick: this.handleCancel },
                            '\u041E\u0442\u043C\u0435\u043D\u0430'
                        )
                    )
                ),
                _react2.default.createElement(_Diag2.default, { model: this.state.diag })
            );
        }
    }]);

    return TicketCard;
}(_react.Component);

exports.default = TicketCard;

/***/ }),

/***/ 877:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _TicketCard = __webpack_require__(868);

var _TicketCard2 = _interopRequireDefault(_TicketCard);

var _store = __webpack_require__(404);

var _store2 = _interopRequireDefault(_store);

var _helper = __webpack_require__(405);

var _helper2 = _interopRequireDefault(_helper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TicketAdd = function (_Component) {
    _inherits(TicketAdd, _Component);

    function TicketAdd() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, TicketAdd);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = TicketAdd.__proto__ || Object.getPrototypeOf(TicketAdd)).call.apply(_ref, [this].concat(args))), _this), _this.componentDidMount = function () {
            _store2.default.dispatch({
                type: 'SET_PAGE_NAME',
                pageName: 'tickets'
            });
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(TicketAdd, [{
        key: 'render',
        value: function render() {

            return _react2.default.createElement(_TicketCard2.default, { isNew: true });
        }
    }]);

    return TicketAdd;
}(_react.Component);

exports.default = TicketAdd;

/***/ })

});