webpackJsonp([9],{

/***/ 859:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _semanticUiReact = __webpack_require__(126);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var UserFIO = function (_Component) {
    _inherits(UserFIO, _Component);

    function UserFIO() {
        _classCallCheck(this, UserFIO);

        return _possibleConstructorReturn(this, (UserFIO.__proto__ || Object.getPrototypeOf(UserFIO)).apply(this, arguments));
    }

    _createClass(UserFIO, [{
        key: 'render',
        value: function render() {
            var user = this.props.user;

            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                    'span',
                    null,
                    user.user_info.name_f
                ),
                ' ',
                _react2.default.createElement(
                    'span',
                    null,
                    user.user_info.name_i
                ),
                ' ',
                _react2.default.createElement(
                    'span',
                    null,
                    user.user_info.name_o
                )
            );
        }
    }]);

    return UserFIO;
}(_react.Component);

exports.default = UserFIO;

/***/ }),

/***/ 866:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _semanticUiReact = __webpack_require__(126);

var _UserFIO = __webpack_require__(859);

var _UserFIO2 = _interopRequireDefault(_UserFIO);

var _store = __webpack_require__(404);

var _store2 = _interopRequireDefault(_store);

var _helper = __webpack_require__(405);

var _helper2 = _interopRequireDefault(_helper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Tickets = function (_Component) {
    _inherits(Tickets, _Component);

    function Tickets() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, Tickets);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Tickets.__proto__ || Object.getPrototypeOf(Tickets)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
            tickets: null,
            column: null,
            direction: null
        }, _this.componentDidMount = function () {
            _store2.default.dispatch({
                type: 'SET_PAGE_NAME',
                pageName: 'tickets'
            });

            _store2.default.subscribe(_this.handleStoreChange);

            _this.getTickets();
        }, _this.handleTicketSelect = function (e) {
            e.preventDefault();

            var t = e.target;
            while (t.tagName != 'TR') {
                t = t.parentNode;
            }

            var ticket_id = t.getAttribute('ticket_id');

            for (var i = 0; i < _this.state.tickets.length; i++) {
                if (_this.state.tickets[i].id == ticket_id) {
                    var ticket = _this.state.tickets[i];

                    _store2.default.dispatch({
                        type: 'SET_SELECTED_TICKET',
                        ticket: ticket
                    });
                    _helper2.default.goToPage('/ticket/' + ticket.id);

                    break;
                }
            }
        }, _this.handleSort = function (clickedColumn) {
            return function () {
                var _this$state = _this.state,
                    column = _this$state.column,
                    tickets = _this$state.tickets,
                    direction = _this$state.direction;


                if (column !== clickedColumn) {
                    _this.setState({
                        column: clickedColumn,
                        tickets: _.sortBy(tickets, [clickedColumn]),
                        direction: 'ascending'
                    });

                    return;
                }

                _this.setState({
                    users: tickets.reverse(),
                    direction: direction === 'ascending' ? 'descending' : 'ascending'
                });
            };
        }, _this.getTickets = function () {
            _helper2.default.http_send({
                url: 'admin/tickets',
                method: 'getAll',
                onSuccess: function onSuccess(data) {
                    _this.setState({
                        tickets: data.items
                    });
                }
            });
        }, _this.handleAdd = function () {
            _helper2.default.goToPage('/ticket_add');
        }, _this.handleClientSelect = function (e, _ref2) {
            var client_id = _ref2.client_id;

            e.preventDefault();
            e.stopPropagation();

            for (var i = 0; i < _this.state.tickets.length; i++) {
                if (_this.state.tickets[i].client.id == client_id) {
                    var user = _this.state.tickets[i].client;

                    _store2.default.dispatch({
                        type: 'SET_SELECTED_USER',
                        user: user
                    });

                    break;
                }
            }

            _helper2.default.goToPage('/user/' + client_id);
        }, _this.handleStoreChange = function () {}, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(Tickets, [{
        key: 'render',
        value: function render() {
            var _this2 = this;

            var _state = this.state,
                column = _state.column,
                tickets = _state.tickets,
                direction = _state.direction;

            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                    _semanticUiReact.Header,
                    { size: 'medium' },
                    '\u0421\u043F\u0438\u0441\u043E\u043A \u0437\u0430\u0434\u0430\u0447  ',
                    _react2.default.createElement(
                        _semanticUiReact.Button,
                        { size: 'mini', color: 'green', onClick: this.handleAdd },
                        '\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C'
                    )
                ),
                _react2.default.createElement(
                    _semanticUiReact.Table,
                    { sortable: true, celled: true, striped: true, selectable: true, fixed: true },
                    _react2.default.createElement(
                        _semanticUiReact.Table.Header,
                        null,
                        _react2.default.createElement(
                            _semanticUiReact.Table.Row,
                            null,
                            _react2.default.createElement(
                                _semanticUiReact.Table.HeaderCell,
                                { sorted: column === 'admin_id' ? direction : null, onClick: this.handleSort('admin_id') },
                                '\u0410\u0434\u043C\u0438\u043D'
                            ),
                            _react2.default.createElement(
                                _semanticUiReact.Table.HeaderCell,
                                { sorted: column === 'client_id' ? direction : null, onClick: this.handleSort('client_id') },
                                '\u041A\u043B\u0438\u0435\u043D\u0442'
                            ),
                            _react2.default.createElement(
                                _semanticUiReact.Table.HeaderCell,
                                { sorted: column === 'title' ? direction : null, onClick: this.handleSort('title') },
                                '\u0417\u0430\u0433\u043E\u043B\u043E\u0432\u043E\u043A'
                            ),
                            _react2.default.createElement(
                                _semanticUiReact.Table.HeaderCell,
                                { sorted: column === 'ticket_status_id' ? direction : null, onClick: this.handleSort('ticket_status_id') },
                                '\u0421\u0442\u0430\u0442\u0443\u0441'
                            )
                        )
                    ),
                    _react2.default.createElement(
                        _semanticUiReact.Table.Body,
                        null,
                        _.map(tickets, function (e, index) {
                            return _react2.default.createElement(
                                _semanticUiReact.Table.Row,
                                { tabIndex: '1', ticket_id: e.id, key: index, onClick: _this2.handleTicketSelect },
                                _react2.default.createElement(
                                    _semanticUiReact.Table.Cell,
                                    null,
                                    e.admin == null ? '---' : _react2.default.createElement(_UserFIO2.default, { user: e.client })
                                ),
                                _react2.default.createElement(
                                    _semanticUiReact.Table.Cell,
                                    null,
                                    e.client == null ? '---' : _react2.default.createElement(
                                        _semanticUiReact.Button,
                                        { compact: true, client_id: e.client.id, onClick: _this2.handleClientSelect },
                                        _react2.default.createElement(_UserFIO2.default, { user: e.client })
                                    )
                                ),
                                _react2.default.createElement(
                                    _semanticUiReact.Table.Cell,
                                    null,
                                    e.title
                                ),
                                _react2.default.createElement(
                                    _semanticUiReact.Table.Cell,
                                    null,
                                    e.ticket_status == null ? '---' : _react2.default.createElement(
                                        _semanticUiReact.Label,
                                        { as: 'a', tag: true },
                                        e.ticket_status.name
                                    )
                                )
                            );
                        })
                    )
                )
            );
        }
    }]);

    return Tickets;
}(_react.Component);

exports.default = Tickets;
;

/***/ })

});