webpackJsonp([8],{

/***/ 857:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _UserCard = __webpack_require__(858);

var _UserCard2 = _interopRequireDefault(_UserCard);

var _store = __webpack_require__(404);

var _store2 = _interopRequireDefault(_store);

var _helper = __webpack_require__(405);

var _helper2 = _interopRequireDefault(_helper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var UserAdd = function (_Component) {
    _inherits(UserAdd, _Component);

    function UserAdd() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, UserAdd);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = UserAdd.__proto__ || Object.getPrototypeOf(UserAdd)).call.apply(_ref, [this].concat(args))), _this), _this.componentDidMount = function () {
            _store2.default.dispatch({
                type: 'SET_PAGE_NAME',
                pageName: 'users'
            });
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(UserAdd, [{
        key: 'render',
        value: function render() {

            return _react2.default.createElement(_UserCard2.default, { isNew: true });
        }
    }]);

    return UserAdd;
}(_react.Component);

exports.default = UserAdd;

/***/ }),

/***/ 858:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _semanticUiReact = __webpack_require__(126);

var _Diag = __webpack_require__(406);

var _Diag2 = _interopRequireDefault(_Diag);

var _store = __webpack_require__(404);

var _store2 = _interopRequireDefault(_store);

var _helper = __webpack_require__(405);

var _helper2 = _interopRequireDefault(_helper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var btnStyle = {
    marginLeft: '10px'
};

var UserCardt = function (_Component) {
    _inherits(UserCardt, _Component);

    function UserCardt() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, UserCardt);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = UserCardt.__proto__ || Object.getPrototypeOf(UserCardt)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
            user: null,
            diag: {
                open: false
            }
        }, _this.componentDidMount = function () {
            if (_this.props.isNew) {
                _this.setState({
                    user: {
                        email: '',
                        password1: '',
                        password2: '',
                        user_info: {
                            name_f: '',
                            name_i: '',
                            name_o: '',
                            site_url: '',
                            company_name: '',
                            phone: ''
                        }
                    }
                });
            } else {
                if (_store2.default.getState().selectedUserState != null) {
                    _this.setState({
                        user: _store2.default.getState().selectedUserState
                    });
                } else {
                    _this.getUser();
                }
            }
        }, _this.getUser = function () {
            _helper2.default.http_send({
                url: 'admin/users',
                method: 'getById',
                send: {
                    id: _this.props.pageId
                },
                onSuccess: function onSuccess(data) {
                    data.item.password1 = data.item.password2 = '';
                    data.item.password1 = '';
                    _this.setState({
                        user: data.item
                    });
                }
            });
        }, _this.handleUserChange = function (e, name) {
            var user = _extends({}, _this.state.user);
            user[name] = e.target.value;

            _this.setState({
                user: user
            });
        }, _this.handleUserInfoChange = function (e, name) {
            var user = _extends({}, _this.state.user);
            user.user_info[name] = e.target.value;

            _this.setState({
                user: user
            });
        }, _this.handleSave = function () {
            var s = '';
            if (_this.state.user.password1 != undefined && _this.state.user.password1.length > 0) {
                if (_this.state.user.password1 != _this.state.user.password2) {
                    s = '[Пароли не совпадают]';
                }
                if (_this.state.user.password1.length < 3) {
                    if (s != '') {
                        s += ', ';
                    }
                    s += ']Длина пароля не может быть меньше 3 символов]';
                }
            }
            if (s == '') {
                _helper2.default.http_send({
                    url: 'admin/users',
                    method: 'saveClient',
                    send: {
                        user: _this.state.user
                    },
                    onSuccess: function onSuccess(data) {
                        var user = _extends({}, _this.state.user);
                        user.password1 = user.password2 = '';

                        _this.setState({
                            user: user
                        });
                    }
                });
            } else {
                _this.setState({
                    diag: {
                        open: true,
                        title: 'Ошибка сохранения',
                        text: s,
                        onClose: function onClose() {
                            _this.setState({
                                diag: {
                                    open: false
                                }
                            });
                        },
                        btns: [{
                            title: 'Понятно',
                            color: 'green',
                            onClick: function onClick() {
                                _this.state.diag.onClose();
                            }
                        }]
                    }
                });
            }
        }, _this.handleDelete = function () {
            _this.setState({
                diag: {
                    open: true,
                    size: 'mini',
                    title: 'Удаление клиента',
                    text: 'Вы уверены что хотите удалить этого клиента?',
                    onClose: function onClose() {
                        _this.setState({
                            diag: {
                                open: false
                            }
                        });
                    },
                    btns: [{
                        title: 'Удалить',
                        color: 'red',
                        onClick: function onClick() {
                            _helper2.default.http_send({
                                url: 'admin/users',
                                method: 'delete',
                                send: {
                                    id: _this.props.pageId
                                },
                                onSuccess: function onSuccess(data) {
                                    _this.state.diag.onClose();
                                    _helper2.default.goToPage('users');
                                }
                            });
                        }
                    }, {
                        title: 'Отмена',
                        color: 'blue',
                        onClick: function onClick() {
                            _this.state.diag.onClose();
                        }
                    }]
                }
            });
        }, _this.handleCreate = function () {
            var user = _this.state.user;

            console.info('handleCreate', user);
            var s = '';
            if (user.user_info.name_f.length < 2 || user.user_info.name_i.length < 2 || user.user_info.name_o.length < 2) {
                s = '[ФИО должно быть задано полностью]';
            }
            if (user.user_info.site_url.length < 3) {
                if (s != '') {
                    s += ', ';
                }
                s = '[ФИО должно быть задано полностью]';
            }
            if (user.password1.length > 0) {
                if (user.password1 != user.password2) {
                    if (s != '') {
                        s += ', ';
                    }
                    s = '[Пароли не совпадают]';
                }
                if (user.password1.length < 3) {
                    if (s != '') {
                        s += ', ';
                    }
                    s += ']Длина пароля не может быть меньше 3 символов]';
                }
            }
            if (s == '') {
                _helper2.default.http_send({
                    url: 'admin/users',
                    method: 'createClient',
                    send: {
                        user: user
                    },
                    onSuccess: function onSuccess(data) {
                        _helper2.default.goToPage('users');
                    }
                });
            } else {
                _this.setState({
                    diag: {
                        open: true,
                        title: 'Ошибка создания',
                        text: s,
                        onClose: function onClose() {
                            _this.setState({
                                diag: {
                                    open: false
                                }
                            });
                        },
                        btns: [{
                            title: 'Понятно',
                            color: 'green',
                            onClick: function onClick() {
                                _this.state.diag.onClose();
                            }
                        }]
                    }
                });
            }
        }, _this.handleCancel = function () {
            _helper2.default.goToPage('users');
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(UserCardt, [{
        key: 'render',
        value: function render() {
            var _this2 = this;

            var isNew = this.props.isNew;
            var user = this.state.user;

            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                    _semanticUiReact.Header,
                    { size: 'medium' },
                    '\u041A\u0430\u0440\u0442\u043E\u0447\u043A\u0430 \u043A\u043B\u0438\u0435\u043D\u0442\u0430'
                ),
                _react2.default.createElement(
                    'div',
                    null,
                    user == null ? _react2.default.createElement(
                        'p',
                        null,
                        '\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430...'
                    ) : _react2.default.createElement(
                        _semanticUiReact.Form,
                        null,
                        _react2.default.createElement(
                            _semanticUiReact.Form.Group,
                            null,
                            _react2.default.createElement(_semanticUiReact.Form.Input, { placeholder: '\u0418\u043C\u044F', value: user.user_info.name_f, width: 6, onChange: function onChange(e) {
                                    return _this2.handleUserInfoChange(e, 'name_f');
                                } }),
                            _react2.default.createElement(_semanticUiReact.Form.Input, { placeholder: '\u0424\u0430\u043C\u0438\u043B\u0438\u044F', value: user.user_info.name_i, width: 6, onChange: function onChange(e) {
                                    return _this2.handleUserInfoChange(e, 'name_i');
                                } }),
                            _react2.default.createElement(_semanticUiReact.Form.Input, { placeholder: '\u041E\u0442\u0447\u0435\u0441\u0442\u0432\u043E', value: user.user_info.name_o, width: 6, onChange: function onChange(e) {
                                    return _this2.handleUserInfoChange(e, 'name_o');
                                } })
                        ),
                        _react2.default.createElement(
                            _semanticUiReact.Form.Group,
                            null,
                            _react2.default.createElement(_semanticUiReact.Form.Input, { placeholder: '\u0421\u0430\u0439\u0442', value: user.user_info.site_url, width: 6, onChange: function onChange(e) {
                                    return _this2.handleUserInfoChange(e, 'site_url');
                                } }),
                            _react2.default.createElement(_semanticUiReact.Form.Input, { placeholder: '\u041A\u043E\u043C\u043F\u0430\u043D\u0438\u044F', value: user.user_info.company_name, width: 6, onChange: function onChange(e) {
                                    return _this2.handleUserInfoChange(e, 'company_name');
                                } }),
                            _react2.default.createElement(_semanticUiReact.Form.Input, { placeholder: '\u0422\u0435\u043B\u0435\u0444\u043E\u043D', value: user.user_info.phone, width: 6, onChange: function onChange(e) {
                                    return _this2.handleUserInfoChange(e, 'phone');
                                } })
                        ),
                        _react2.default.createElement(
                            _semanticUiReact.Form.Group,
                            null,
                            _react2.default.createElement(_semanticUiReact.Form.Input, { placeholder: 'Email', value: user.email, width: 6, onChange: function onChange(e) {
                                    return _this2.handleUserChange(e, 'email');
                                } }),
                            _react2.default.createElement(_semanticUiReact.Form.Input, { placeholder: '\u041D\u043E\u0432\u044B\u0439 \u043F\u0430\u0440\u043E\u043B\u044C', value: user.password1, width: 6, onChange: function onChange(e) {
                                    return _this2.handleUserChange(e, 'password1');
                                } }),
                            _react2.default.createElement(_semanticUiReact.Form.Input, { placeholder: '\u041D\u043E\u0432\u044B\u0439 \u043F\u0430\u0440\u043E\u043B\u044C \u0435\u0449\u0435 \u0440\u0430\u0437', value: user.password2, width: 6, onChange: function onChange(e) {
                                    return _this2.handleUserChange(e, 'password2');
                                } })
                        )
                    )
                ),
                _react2.default.createElement(
                    'div',
                    null,
                    isNew ? _react2.default.createElement(
                        'span',
                        null,
                        _react2.default.createElement(
                            _semanticUiReact.Button,
                            { basic: true, color: 'green', onClick: this.handleCreate },
                            '\u0421\u043E\u0437\u0434\u0430\u0442\u044C'
                        ),
                        _react2.default.createElement(
                            _semanticUiReact.Button,
                            { style: btnStyle, basic: true, color: 'blue', onClick: this.handleCancel },
                            '\u041E\u0442\u043C\u0435\u043D\u0430'
                        )
                    ) : _react2.default.createElement(
                        'span',
                        null,
                        _react2.default.createElement(
                            _semanticUiReact.Button,
                            { basic: true, color: 'green', onClick: this.handleSave },
                            '\u0421\u043E\u0445\u0440\u0430\u043D\u0438\u0442\u044C'
                        ),
                        _react2.default.createElement(
                            _semanticUiReact.Button,
                            { style: btnStyle, basic: true, color: 'red', onClick: this.handleDelete },
                            '\u0423\u0434\u0430\u043B\u0438\u0442\u044C'
                        ),
                        _react2.default.createElement(
                            _semanticUiReact.Button,
                            { style: btnStyle, basic: true, color: 'blue', onClick: this.handleCancel },
                            '\u041E\u0442\u043C\u0435\u043D\u0430'
                        )
                    )
                ),
                _react2.default.createElement(_Diag2.default, { model: this.state.diag })
            );
        }
    }]);

    return UserCardt;
}(_react.Component);

exports.default = UserCardt;

/***/ })

});