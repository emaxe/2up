<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TicketController extends WrapController
{
    use TraitAdminController;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->response['class'] = static::class;
        $this->model = new \App\Ticket();
    }

    private function getAll(){
        $items = [];


        if(Auth::user()->hasRole('admin')){
            $items = $this->model
                ->with(['client.user_info', 'admin.user_info', 'ticket_status'])
                ->get();
        }elseif(Auth::user()->hasRole('client')){
            $items = $this->model
                ->whereHas('client.user_info', function($q){
                    $q->where('company_name', Auth::user()->user_info->company_name);
                })
                ->with(['client.user_info', 'admin.user_info', 'ticket_status'])
                ->get();
        }



        $this->response['items'] = $items;
        $this->response['action'] = 'getAll';
        $this->response['state'] = $this->state;
        return response()->json($this->response)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    private function getById($request){
        $item = null;

        if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('client')){
            if($request->has('id')){
                $ticket = $this->model->with('ticket_status', 'admin')->find($request->get('id'));
                if($ticket != null){
                    if(Auth::user()->hasRole('client')){
                        if($ticket->client->user_info->company_name == Auth::user()->user_info->company_name){
                            $item = $ticket;
                        }else{
                            $this->state = 4;
                        }
                    }else{
                        $item = $ticket;
                    }
                }else{
                    $this->state = 3;
                }
            }else{
                $this->state = 2;
            }
        }else{
            $this->state = 1;
        }

        $this->response['item'] = $item;
        $this->response['action'] = 'getById';
        $this->response['state'] = $this->state;
        return response()->json($this->response)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    private function getTicketStatuses(){
        $items = [];

        $items = \App\TicketStatus::all();

        $this->response['items'] = $items;
        $this->response['action'] = 'getTicketStatuses';
        $this->response['state'] = $this->state;
        return response()->json($this->response)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    private function createTicket($request){
        $item = null;

        if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('client')){
            if($request->has('ticket')){
                $r_ticket = $request->get('ticket');
                $this->log(strlen($r_ticket['title']));
                if(Auth::user()->hasRole('client')){
                    $r_ticket['client_id'] = Auth::user()->id;
                    $r_ticket['ticket_status_id'] = 1;
                }
                if(strlen($r_ticket['title']) >= 3 && $r_ticket['client_id']>0 && $r_ticket['ticket_status_id']>0){
                    $item = $this->model->create($r_ticket);
                }else{
                    $this->state = 3;
                }
            }else{
                $this->state = 2;
            }
        }else{
            $this->state = 1;
        }

        $this->response['item'] = $item;
        $this->response['action'] = 'createTicket';
        $this->response['state'] = $this->state;
        return response()->json($this->response)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    private function saveTicket($request){
        $item = null;

        if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('client')){
            if($request->has('ticket')){
                $r_ticket = $request->get('ticket');
                $item = $this->model->find($r_ticket['id']);
                if($item != null){
                    $r_ticket['admin_id'] = Auth::user()->id;
                    $item->update($r_ticket);
                }else{
                    $this->state = 3;
                }
            }else{
                $this->state = 2;
            }
        }else{
            $this->state = 1;
        }

        $this->response['item'] = $item;
        $this->response['action'] = 'save';
        $this->response['state'] = $this->state;
        return response()->json($this->response)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    private function delete($request){
        $item = null;

        if(Auth::user()->hasRole('admin')){
            if($request->has('id')){
                $item = $this->model->find($request->get('id'));
                if($item != null){
                    $item->delete();
                }else{
                    $this->state = 3;
                }
            }else{
                $this->state = 2;
            }
        }else{
            $this->state = 1;
        }

        $this->response['item'] = $item;
        $this->response['action'] = 'delete';
        $this->response['state'] = $this->state;
        return response()->json($this->response)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }
}
