<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends WrapController
{
    use TraitAdminController;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->response['class'] = static::class;
        $this->model = new \App\User();
    }

    private function whoAmI(){
        $item = Auth::user();
        $isAdmin = $item->hasRole('admin');

        $this->response['isAdmin'] = $isAdmin;
        $this->response['item'] = $item;
        $this->response['action'] = 'whoAmI';
        $this->response['state'] = $this->state;
        return response()->json($this->response)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    private function createClient($request){
        $item = null;

        if(Auth::user()->hasRole('admin')){
            if($request->has('user')){
                $r_user = $request->get('user');
                if($this->model->where('email', $r_user['email'])->first() == null){
                    $r_user['name'] = $r_user['user_info']['name_f'];
                    if($r_user['password1'] != '' && $r_user['password1'] == $r_user['password2']){
                        $r_user['password'] = bcrypt($r_user['password1']);
                    }
                    $item = $this->model->create($r_user);
                    $item->attachRole(\App\Role::where('name', 'client')->first()->id);
                    $r_user['user_info']['user_id'] = $item->id;
                    $user_info = \App\UserInfo::create($r_user['user_info']);
                }else{
                    $this->state = 3;
                }
            }else{
                $this->state = 2;
            }
        }else{
            $this->state = 1;
        }

        $this->response['item'] = $item;
        $this->response['action'] = 'createClient';
        $this->response['state'] = $this->state;
        return response()->json($this->response)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    private function getById($request){
        $item = null;

        if(Auth::user()->hasRole('admin')){
            if($request->has('id')){
                $item = $this->model->with('user_info')->find($request->get('id'));
                if($item == null){
                    $this->state = 3;
                }
            }else{
                $this->state = 2;
            }
        }else{
            $this->state = 1;
        }

        $this->response['item'] = $item;
        $this->response['action'] = 'getById';
        $this->response['state'] = $this->state;
        return response()->json($this->response)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    private function saveClient($request){
        $item = null;

        if(Auth::user()->hasRole('admin')){
            if($request->has('user')){
                $r_user = $request->get('user');
                $item = $this->model->find($r_user['id']);
                if($item != null){
                    $user_info = $item->user_info()->first();
                    if($user_info != null){
                        $user_info->update($r_user['user_info']);
                    }
                    if(isset($r_user['password1']) && isset($r_user['password12']) && $r_user['password1'] != '' && $r_user['password1'] == $r_user['password2']){
                        $r_user['password'] = bcrypt($r_user['password1']);
                    }
                    $item->update($r_user);
                }else{
                    $this->state = 3;
                }
            }else{
                $this->state = 2;
            }
        }else{
            $this->state = 1;
        }

        $this->response['item'] = $item;
        $this->response['action'] = 'save';
        $this->response['state'] = $this->state;
        return response()->json($this->response)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    private function delete($request){
        $item = null;

        if(Auth::user()->hasRole('admin')){
            if($request->has('id')){
                $item = $this->model->with('user_info')->find($request->get('id'));
                if($item != null){
                    $user_info = $item->user_info()->first();
                    if($user_info != null){
                        $user_info->delete();
                    }
                    $item->delete();
                }else{
                    $this->state = 3;
                }
            }else{
                $this->state = 2;
            }
        }else{
            $this->state = 1;
        }

        $this->response['item'] = $item;
        $this->response['action'] = 'delete';
        $this->response['state'] = $this->state;
        return response()->json($this->response)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    private function getClients($request){
        $items = [];

        if(Auth::user()->hasRole('admin')){
            $items = $this->model
                ->whereHas('roles', function($q){
                    $q->where('name', 'client');
                })
                ->with('user_info')
//                ->with('roles')
                ->get();
        }else{
            $this->state = 1;
        }

        $this->response['items'] = $items;
        $this->response['action'] = 'getClients';
        $this->response['state'] = $this->state;
        return response()->json($this->response)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    private function getClientsAndAdmins($request){
        $clients = [];
        $admins = [];

        if(Auth::user()->hasRole('admin')){
            $clients = $this->model
                ->whereHas('roles', function($q){
                    $q->where('name', 'client');
                })
                ->with('user_info')
                ->get();

            $admins = $this->model
                ->whereHas('roles', function($q){
                    $q->where('name', 'admin');
                })
                ->with('user_info')
                ->get();
        }else{
            $this->state = 1;
        }

        $this->response['clients'] = $clients;
        $this->response['admins'] = $admins;
        $this->response['action'] = 'getClientsAndAdmin';
        $this->response['state'] = $this->state;
        return response()->json($this->response)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }
}
