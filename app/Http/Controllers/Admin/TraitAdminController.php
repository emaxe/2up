<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

trait TraitAdminController{
    public function index(Request $request){

        if($request->has('method')){
            $method = $request->get('method');
            if(method_exists($this, $method)){
                $this->$method($request);
            }else{
                $this->state = -2;
            }
        }else{
            $this->state = -1;
        }
        $this->response['state'] = $this->state;
        return response()->json($this->response)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }
}
