<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CommentController extends WrapController
{
    use TraitAdminController;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->response['class'] = static::class;
        $this->model = new \App\Comment();
    }

    private function getByTicketId($request){
        $items = [];

        if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('client')){
            if($request->has('id')){
                $ticket = \App\Ticket::find($request->get('id'));
                if($ticket != null){
                    $items = $ticket->comments()->with(['user.user_info', 'user.roles'])->get();
                    foreach ($items as $item){
                        $this->prepareComment($item);
                    }
                }else{
                    $this->state = 3;
                }
            }else{
                $this->state = 2;
            }
        }else{
            $this->state = 1;
        }

        $this->response['items'] = $items;
        $this->response['action'] = 'getByTicketId';
        $this->response['state'] = $this->state;
        return response()->json($this->response)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    private function prepareComment(\App\Comment $comment){
        $children = $comment->children()->with(['user.user_info', 'user.roles'])->get();
        if(count($children)>0){
            foreach ($children as $child){
                $this->prepareComment($child);
            }
        }
        $comment->children = $children;
    }

    private function addCommentToTicket($request){
        $item = null;

        if($request->has('ticketId') && $request->has('text')){
            $ticket = \App\Ticket::find($request->get('ticketId'));
            if($ticket != null){
                $f = true;
                if(Auth::user()->hasRole('client') && $ticket->client->user_info()->first()->company_name != Auth::user()->user_info()->first()->company_name){
                    $f = false;
                }
                if($f){
                    $item = $this->model->create([
                        'ticket_id' => $ticket->id,
                        'user_id' => Auth::user()->id,
                        'text' => $request->get('text'),
                    ]);
                }else{
                    $this->state = 3;
                }
            }else{
                $this->state = 2;
            }
        }else{
            $this->state = 1;
        }

        $this->response['item'] = $item;
        $this->response['action'] = 'addComment';
        $this->response['state'] = $this->state;
        return response()->json($this->response)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    private function addCommentToComment($request){
        $item = null;

        if($request->has('commentId') && $request->has('text')){
            $comment = $this->model->find($request->get('commentId'));
            if($comment != null){
//                $f = true;
//                if(Auth::user()->hasRole('client')){
//                    $pComment = $comment->parent()->first();
//                    while ($pComment->ticket()->first() == null){
//                        $pComment = $pComment->parent()->first();
//                    }
//                    if($pComment->ticket()->first()->client->user_info()->first()->company_name != Auth::user()->user_info()->first()->company_name){
//                        $f = false;
//                    }
//                }
//                if($f){
                    $item = $this->model->create([
                        'pid' => $comment->id,
                        'user_id' => Auth::user()->id,
                        'text' => $request->get('text'),
                    ]);
//                }else{
//                    $this->state = 3;
//                }
            }else{
                $this->state = 2;
            }
        }else{
            $this->state = 1;
        }

        $this->response['item'] = $item;
        $this->response['action'] = 'addComment';
        $this->response['state'] = $this->state;
        return response()->json($this->response)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }
}
