<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class WrapController extends Controller
{
    public $response;
    public $request;
    public $state;
    public $that;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->response = [
            'user' => Auth::user(),
            'request' => $request->all(),
            'log' => []
        ];

        $this->state = 0;
    }

    public function log(){
        $o = null;
        if(func_num_args()>1){
            $array = [];
            foreach(func_get_args() as $arg){
                $array[] = $arg;
            }
            $o = $array;
        }else{
            $o = func_get_arg(0);
        }
        $this->response['log'][] = $o;
    }

    public function is_assoc($array){
        return count(array_filter(array_keys($array), 'is_string')) > 0;
    }
}
