<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index(Request $request){
//        if(Auth::user()->hasRole('client')){
//            $user_info = Auth::user()->user_info;
//            $user_info->last_login = Carbon::now()->toDateTimeString();
//            $user_info->save();
//        }

        return view('admin.index');
    }
}
