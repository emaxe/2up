<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    protected $fillable = [
        'user_id',
        'company_name',
        'name_f',
        'name_i',
        'name_o',
        'site_url',
        'phone',
        'description',
        'last_login',
    ];
    //-----------------------------------

    //-----------------------------------

    public function user(){
        return $this->belongsTo(\App\User::class);
    }
}
