<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'client_id',
        'admin_id',
        'ticket_status_id',
        'title',
        'description',
    ];
    //-----------------------------------

    //-----------------------------------

    public function client(){
        return $this->belongsTo(\App\User::class, 'client_id');
    }

    public function admin(){
        return $this->belongsTo(\App\User::class, 'admin_id');
    }

    public function ticket_status(){
        return $this->belongsTo(\App\TicketStatus::class);
    }

    public function comments(){
        return $this->hasMany(\App\Comment::class);
    }
}
