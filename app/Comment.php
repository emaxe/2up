<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'text',
        'pid',
        'user_id',
        'ticket_id',
    ];
    //-----------------------------------

    //-----------------------------------

    public function parent(){
        return $this->belongsTo(\App\Comment::class, 'pid');
    }

    public function children(){
        return $this->hasMany(\App\Comment::class, 'pid');
    }

    public function user(){
        return $this->belongsTo(\App\User::class);
    }

    public function ticket(){
        return $this->belongsTo(\App\Ticket::class);
    }
}
