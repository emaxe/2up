import React from 'react';
import ReactDom from 'react-dom';
import { render } from 'react-dom';
import { HashRouter } from 'react-router-dom';
import ScreenComponent from './components/ScreenComponent';


import './bootstrap'
import 'typeface-roboto'


const App = () => (
    <ScreenComponent />
);

ReactDom.render(
    <HashRouter>
        <App />
    </HashRouter>
    ,
    document.getElementById('app')
);