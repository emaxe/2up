import React, { Component } from 'react';
import { Button, Image, List, Table, Header } from 'semantic-ui-react'

import UserFIO from './UserFIO'

import store from '../../stores/store';
import helper from '../../services/helper';

export default class Users extends Component{
    state = {
        users: null,
        column: null,
        direction: null,
    };

    componentDidMount = () => {
        store.dispatch({
            type: 'SET_PAGE_NAME',
            pageName: 'users',
        });

        store.subscribe(this.handleStoreChange);

        this.getUsers();
    };


    handleUserSelect = (user) => {
        store.dispatch({
            type: 'SET_SELECTED_USER',
            user: user,
        });
        helper.goToPage('/user/'+user.id);
    };

    handleSort = clickedColumn => () => {
        const { column, users, direction } = this.state

        if (column !== clickedColumn) {
            this.setState({
                column: clickedColumn,
                // users: _.sortBy(users, [clickedColumn]),
                users: _.sortBy(users, [o => {
                    if(clickedColumn.charAt(0) == '_'){
                        if(clickedColumn == '_fio'){
                            return [o.user_info.name_f, o.user_info.name_i, o.user_info.name_o];
                        }else{
                            return o.user_info[clickedColumn.slice(1)];
                        }
                    }else{
                        return clickedColumn;
                    }
                }]),
                direction: 'ascending',
            });

            return
        }

        this.setState({
            users: users.reverse(),
            direction: direction === 'ascending' ? 'descending' : 'ascending',
        })
    };

    getUsers = () => {
        helper.http_send({
            url: 'admin/users',
            method: 'getClients',
            onSuccess: (data) => {
                this.setState({
                    users: data.items,
                })
            }
        })
    };

    handleAdd = () => {
        helper.goToPage('/user_add');
    };

    handleStoreChange = () => {

    };


    render(){
        const { column, users, direction } = this.state;
        return (
            <div>
                <Header size='medium'>Список клиентов  <Button size='mini' color="green" onClick={this.handleAdd}>Добавить</Button></Header>

                <Table sortable celled striped selectable fixed>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell sorted={column === '_fio' ? direction : null} onClick={this.handleSort('_fio')}>
                                ФИО
                            </Table.HeaderCell>
                            <Table.HeaderCell sorted={column === 'email' ? direction : null} onClick={this.handleSort('email')}>
                                Email
                            </Table.HeaderCell>
                            <Table.HeaderCell sorted={column === '_site_url' ? direction : null} onClick={this.handleSort('_site_url')}>
                                Сайт
                            </Table.HeaderCell>
                            <Table.HeaderCell sorted={column === '_company_name' ? direction : null} onClick={this.handleSort('_company_name')}>
                                Компания
                            </Table.HeaderCell>
                            <Table.HeaderCell sorted={column === '_phone' ? direction : null} onClick={this.handleSort('_phone')}>
                                Телефон
                            </Table.HeaderCell>
                            <Table.HeaderCell sorted={column === '_last_login' ? direction : null} onClick={this.handleSort('_last_login')}>
                                Последний визит
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {_.map(users, (e, index) => (
                            <Table.Row  key={index} onClick={ () => this.handleUserSelect(e)}>
                                <Table.Cell><UserFIO user={e}/></Table.Cell>
                                <Table.Cell>{e.email}</Table.Cell>
                                <Table.Cell>{e.user_info.site_url}</Table.Cell>
                                <Table.Cell>{e.user_info.company_name}</Table.Cell>
                                <Table.Cell>{e.user_info.phone}</Table.Cell>
                                <Table.Cell>{e.user_info.last_login}</Table.Cell>
                            </Table.Row>
                        ))}
                    </Table.Body>
                </Table>

            </div>
        )
    }
};