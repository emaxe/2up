import React, { Component } from 'react';

import UserCard from './UserCard';
import store from '../../stores/store';
import helper from '../../services/helper';

export default class UserAdd extends Component{

    componentDidMount = () => {
        store.dispatch({
            type: 'SET_PAGE_NAME',
            pageName: 'users',
        });
    };

    render(){

        return (
            <UserCard isNew={true}/>
        )
    }
}