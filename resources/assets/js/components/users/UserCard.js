import React, { Component } from 'react';
import { Button, Image, List, Table, Card, Form, Header } from 'semantic-ui-react';

import Diag from '../vendor/Diag';
import store from '../../stores/store';
import helper from '../../services/helper';

const btnStyle = {
    marginLeft: '10px',
};

export default class UserCardt extends Component{
    state = {
        user: null,
        diag: {
            open: false,
        }
    };

    componentDidMount = () => {
        if(this.props.isNew){
            this.setState({
                user: {
                    email: '',
                    password1: '',
                    password2: '',
                    user_info: {
                        name_f: '',
                        name_i: '',
                        name_o: '',
                        site_url: '',
                        company_name: '',
                        phone: '',
                    }
                }
            });
        }else{
            if(store.getState().selectedUserState != null){
                this.setState({
                    user: store.getState().selectedUserState,
                });
            }else{
                this.getUser();
            }
        }
    };

    getUser = () => {
        helper.http_send({
            url: 'admin/users',
            method: 'getById',
            send: {
                id: this.props.pageId,
            },
            onSuccess: (data) => {
                data.item.password1 = data.item.password2 = '';
                data.item.password1 = '';
                this.setState({
                    user: data.item,
                })
            }
        })
    };

    handleUserChange = (e,name) => {
        let user = {...this.state.user};
        user[name] = e.target.value;

        this.setState({
            user: user,
        });
    };

    handleUserInfoChange = (e,name) => {
        let user = {...this.state.user};
        user.user_info[name] = e.target.value;

        this.setState({
            user: user,
        });

    };

    handleSave = () => {
        var s = '';
        if(this.state.user.password1!=undefined && this.state.user.password1.length > 0){
            if(this.state.user.password1 != this.state.user.password2){
                s = '[Пароли не совпадают]';
            }
            if(this.state.user.password1.length<3){
                if(s!=''){
                    s +=', ';
                }
                s += ']Длина пароля не может быть меньше 3 символов]';
            }
        }
        if(s == ''){
            helper.http_send({
                url: 'admin/users',
                method: 'saveClient',
                send: {
                    user: this.state.user,
                },
                onSuccess: (data) => {
                    let user = {...this.state.user};
                    user.password1 = user.password2 = '';

                    this.setState({
                        user: user,
                    });
                }
            })
        }else{
            this.setState({
                diag: {
                    open: true,
                    title: 'Ошибка сохранения',
                    text: s,
                    onClose: () => {
                        this.setState({
                            diag: {
                                open: false,
                            }
                        })
                    },
                    btns: [
                        {
                            title: 'Понятно',
                            color: 'green',
                            onClick: () => {
                                this.state.diag.onClose();
                            }
                        }
                    ]
                }
            });
        }
    };

    handleDelete = () => {
        this.setState({
            diag: {
                open: true,
                size: 'mini',
                title: 'Удаление клиента',
                text: 'Вы уверены что хотите удалить этого клиента?',
                onClose: () => {
                    this.setState({
                        diag: {
                            open: false,
                        }
                    })
                },
                btns: [
                    {
                        title: 'Удалить',
                        color: 'red',
                        onClick: () => {
                            helper.http_send({
                                url: 'admin/users',
                                method: 'delete',
                                send: {
                                    id: this.props.pageId,
                                },
                                onSuccess: (data) => {
                                    this.state.diag.onClose();
                                    helper.goToPage('users');
                                }
                            })
                        }
                    },
                    {
                        title: 'Отмена',
                        color: 'blue',
                        onClick: () => {
                            this.state.diag.onClose();
                        }
                    }
                ]
            }
        })
    };

    handleCreate = () => {
        let {user} = this.state;
        console.info('handleCreate', user);
        var s = '';
        if(user.user_info.name_f.length < 2 || user.user_info.name_i.length < 2 || user.user_info.name_o.length < 2){
            s = '[ФИО должно быть задано полностью]';
        }
        if(user.user_info.site_url.length<3){
            if(s!=''){
                s +=', ';
            }
            s = '[ФИО должно быть задано полностью]';
        }
        if(user.password1.length > 0){
            if(user.password1 != user.password2){
                if(s!=''){
                    s +=', ';
                }
                s = '[Пароли не совпадают]';
            }
            if(user.password1.length<3){
                if(s!=''){
                    s +=', ';
                }
                s += ']Длина пароля не может быть меньше 3 символов]';
            }
        }
        if(s == ''){
            helper.http_send({
                url: 'admin/users',
                method: 'createClient',
                send: {
                    user: user,
                },
                onSuccess: (data) => {
                    helper.goToPage('users');
                }
            })
        }else{
            this.setState({
                diag: {
                    open: true,
                    title: 'Ошибка создания',
                    text: s,
                    onClose: () => {
                        this.setState({
                            diag: {
                                open: false,
                            }
                        })
                    },
                    btns: [
                        {
                            title: 'Понятно',
                            color: 'green',
                            onClick: () => {
                                this.state.diag.onClose();
                            }
                        }
                    ]
                }
            });
        }
    };

    handleCancel = () => {
        helper.goToPage('users');
    };

    render(){
        const {isNew} = this.props;
        const {user} = this.state;
        return (
            <div>
                <Header size='medium'>Карточка клиента</Header>

                <div>
                    {user==null ? (
                        <p>Загрузка...</p>
                    ) : (
                        <Form>
                            <Form.Group>
                                <Form.Input placeholder='Имя' value={user.user_info.name_f} width={6} onChange={e => this.handleUserInfoChange(e,'name_f')} />
                                <Form.Input placeholder='Фамилия' value={user.user_info.name_i} width={6} onChange={e => this.handleUserInfoChange(e,'name_i')} />
                                <Form.Input placeholder='Отчество' value={user.user_info.name_o} width={6} onChange={e => this.handleUserInfoChange(e,'name_o')} />
                            </Form.Group>
                            <Form.Group>
                                <Form.Input placeholder='Сайт' value={user.user_info.site_url} width={6} onChange={e => this.handleUserInfoChange(e,'site_url')} />
                                <Form.Input placeholder='Компания' value={user.user_info.company_name} width={6} onChange={e => this.handleUserInfoChange(e,'company_name')} />
                                <Form.Input placeholder='Телефон' value={user.user_info.phone} width={6} onChange={e => this.handleUserInfoChange(e,'phone')} />
                            </Form.Group>
                            <Form.Group>
                                <Form.Input placeholder='Email' value={user.email} width={6} onChange={e => this.handleUserChange(e,'email')} />
                                <Form.Input placeholder='Новый пароль' value={user.password1} width={6} onChange={e => this.handleUserChange(e,'password1')} />
                                <Form.Input placeholder='Новый пароль еще раз' value={user.password2} width={6} onChange={e => this.handleUserChange(e,'password2')} />
                            </Form.Group>
                        </Form>
                    )}
                </div>

                <div>
                    {isNew ? (
                        <span>
                                  <Button basic color='green' onClick={this.handleCreate}>Создать</Button>
                                  <Button style={btnStyle} basic color='blue' onClick={this.handleCancel}>Отмена</Button>
                              </span>
                    ) : (
                        <span>
                                    <Button basic color='green' onClick={this.handleSave}>Сохранить</Button>
                                    <Button style={btnStyle} basic color='red' onClick={this.handleDelete}>Удалить</Button>
                                    <Button style={btnStyle} basic color='blue' onClick={this.handleCancel}>Отмена</Button>
                                </span>
                    )}
                </div>

                <Diag model={this.state.diag} />
            </div>
        )
    }
}