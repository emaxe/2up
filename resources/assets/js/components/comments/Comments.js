import React, { Component } from 'react';
import { Button, Comment, Form, Header, Modal } from 'semantic-ui-react';

import CommentItem from './CommentItem';

import store from '../../stores/store';
import helper from '../../services/helper';

export default class Comments extends Component {
    state = {
        comments: [],
        commentText: '',
        open: false,
    };

    componentDidMount = () => {
        this.getComments();
    };

    getComments = () => {
        const {ticketId} = this.props;

        helper.http_send({
            url: 'admin/comments',
            method: 'getByTicketId',
            send: {
                id: ticketId
            },
            onSuccess: (data) => {
                this.setState({
                    comments: data.items,
                })
            }
        })
    };

    handleOpen = () => {
        this.setState({
            open: true,
        })
    };

    handleClose = () => {
        this.setState({
            open: false,
        })
    };

    handleAddComment = (e) => {
        const {commentText} = this.state;
        const {ticketId} = this.props;

        if(commentText != ''){
            helper.http_send({
                url: 'admin/comments',
                method: 'addCommentToTicket',
                send: {
                    text: _.truncate(commentText, {
                        length: 140,
                    }),
                    ticketId: ticketId,
                },
                onSuccess: (data) => {
                    this.getComments();
                    this.setState({
                        commentText: '',
                        open: false,
                    })
                }
            })
        }
    };

    handleCommentChange = (e) => {
        let value = e.target.value;
        this.setState({
            commentText: _.truncate(value, {
                length: 140,
            })
        })
        // this.initChange(value)
    };

    handleUpdate = () => {
        this.getComments();
    };

    // initChange = _.debounce((e) => {
    //     this.setState({
    //         commentText: e
    //     })
    // },200);

    render() {
        const {comments, commentText, open} = this.state;

        return (
            <div>
                <Comment.Group minimal>
                    <Header as='h3' dividing>Комментарии</Header>

                    {_.map(comments, (e, index) => (
                        <CommentItem comment={e} key={index} update={this.handleUpdate}/>
                    ))}

                    {comments.length == 0 && (
                        <p>Комментарии отсутсвуют...</p>
                    )}

                    <Form reply>
                        <Button content='Добавить комментарий' labelPosition='left' icon='edit' primary onClick={this.handleOpen}/>
                    </Form>

                    <Modal open={open} basic closeIcon onClose={this.handleClose}>
                        <Modal.Content>
                            <Modal.Description>
                                <Header>Введите комментарий</Header>
                                <Form reply>
                                    <Form.TextArea value={commentText} onChange={this.handleCommentChange} />
                                    <Button content='Добавить комментарий' labelPosition='left' icon='edit' primary onClick={this.handleAddComment}/>
                                </Form>
                            </Modal.Description>
                        </Modal.Content>
                    </Modal>

                    {/*<Form reply>*/}
                        {/*<Form.TextArea value={commentText} onChange={this.handleCommentChange} />*/}
                        {/*<Button content='Добавить комментарий' labelPosition='left' icon='edit' primary onClick={this.handleAddComment}/>*/}
                    {/*</Form>*/}
                </Comment.Group>
            </div>
        )
    }
}

