import React, { Component } from 'react'
import { Button, Comment, Form, Header, Modal } from 'semantic-ui-react'

import UserFIO from '../users/UserFIO'

import store from '../../stores/store';
import helper from '../../services/helper'

export default class CommentItem extends Component {
    state = {
        commentText: '',
        open: false,
    };

    handleOpen = () => {
        this.setState({
            open: true,
        })
    };

    handleClose = () => {
        this.setState({
            open: false,
        })
    };

    handleAddComment = (e) => {
        const {commentText} = this.state;
        const {comment} = this.props;

        if(commentText != ''){
            helper.http_send({
                url: 'admin/comments',
                method: 'addCommentToComment',
                send: {
                    text: _.truncate(commentText, {
                        length: 140,
                    }),
                    commentId: comment.id,
                },
                onSuccess: (data) => {
                    this.props.update();
                    this.setState({
                        commentText: '',
                        open: false,
                    })
                }
            })
        }
    };

    handleCommentChange = (e) => {
        let value = e.target.value;
        this.setState({
            commentText: _.truncate(value, {
                length: 140,
            })
        })
    };

    handleUpdate = () => {
        this.props.update();
    };

    handleSelectUser = (user) => {
        if(store.getState().userState.isAdmin && user.roles[0].name=='client'){
            helper.goToPage('users/'+user.id);
        }
    };

    render() {
        const {comment} = this.props;
        const {commentText, open} = this.state;
        return (
            <div>
                <Comment>
                    <Comment.Content>
                        <Comment.Author as='a' onClick={() => this.handleSelectUser(comment.user)}>
                            {comment.user.roles[0].name=='admin' ? (
                                <span>{comment.user.email}</span>
                            ) : (
                                <UserFIO user={comment.user} />
                            )}
                        </Comment.Author>
                        <Comment.Metadata>
                            <span>{comment.created_at}</span>
                        </Comment.Metadata>
                        <Comment.Text>{comment.text}</Comment.Text>
                        <Comment.Actions>
                            <a onClick={this.handleOpen}>Ответить</a>
                            <Modal open={open} basic closeIcon onClose={this.handleClose}>
                                <Modal.Content>
                                    <Modal.Description>
                                        <Header>Введите комментарий</Header>
                                        <Form reply>
                                            <Form.TextArea value={commentText} onChange={this.handleCommentChange} />
                                            <Button content='Добавить комментарий' labelPosition='left' icon='edit' primary onClick={this.handleAddComment}/>
                                        </Form>
                                    </Modal.Description>
                                </Modal.Content>
                            </Modal>
                        </Comment.Actions>
                    </Comment.Content>


                    {comment.children.length>0 && (
                        <Comment.Group>
                            {_.map(comment.children, (e, index) => (
                                <CommentItem comment={e} key={index} update={this.handleUpdate}/>
                            ))}
                        </Comment.Group>
                    )}

                </Comment>

            </div>
        )
    }
}

