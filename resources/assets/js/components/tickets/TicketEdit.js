import React, { Component } from 'react';
import { Segment } from 'semantic-ui-react'

import TicketCard from './TicketCard';
import Comments from '../comments/Comments';

import store from '../../stores/store';
import helper from '../../services/helper';

export default class TicketEdit extends Component{

    componentDidMount = () => {
        store.dispatch({
            type: 'SET_PAGE_NAME',
            pageName: 'tickets',
        });
    };

    render(){
        const {id} = this.props.match.params;
        return (
            <div>
                <TicketCard isNew={false} ticketId={id} />

                <Segment raised>
                    <Comments ticketId={id} />
                </Segment>

            </div>
        )
    }
}