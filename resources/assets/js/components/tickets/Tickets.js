import React, { Component } from 'react';
import { Button, Image, List, Table, Header, Label } from 'semantic-ui-react'

import UserFIO from '../users/UserFIO';

import store from '../../stores/store';
import helper from '../../services/helper';

export default class Tickets extends Component{
    state = {
        tickets: null,
        column: null,
        direction: null,
    };

    componentDidMount = () => {
        store.dispatch({
            type: 'SET_PAGE_NAME',
            pageName: 'tickets',
        });

        store.subscribe(this.handleStoreChange);

        this.getTickets();
    };


    handleTicketSelect = (e) => {
        e.preventDefault();

        let t = e.target;
        while (t.tagName != 'TR'){
            t = t.parentNode;
        }

        const ticket_id = t.getAttribute('ticket_id');

        for(var i=0; i<this.state.tickets.length;i++){
            if(this.state.tickets[i].id == ticket_id){
                const ticket = this.state.tickets[i];

                store.dispatch({
                    type: 'SET_SELECTED_TICKET',
                    ticket: ticket,
                });
                helper.goToPage('/ticket/'+ticket.id);

                break;
            }
        }
    };

    handleSort = clickedColumn => () => {
        const { column, tickets, direction } = this.state;

        if (column !== clickedColumn) {
            this.setState({
                column: clickedColumn,
                tickets: _.sortBy(tickets, [clickedColumn]),
                direction: 'ascending',
            });

            return
        }

        this.setState({
            users: tickets.reverse(),
            direction: direction === 'ascending' ? 'descending' : 'ascending',
        })
    };

    getTickets = () => {
        helper.http_send({
            url: 'admin/tickets',
            method: 'getAll',
            onSuccess: (data) => {
                this.setState({
                    tickets: data.items,
                })
            }
        })
    };

    handleAdd = () => {
        helper.goToPage('/ticket_add');
    };

    handleClientSelect = (e, {client_id}) => {
        e.preventDefault();
        e.stopPropagation();

        for(var i=0; i<this.state.tickets.length;i++){
            if(this.state.tickets[i].client.id == client_id){
                const user = this.state.tickets[i].client;

                store.dispatch({
                    type: 'SET_SELECTED_USER',
                    user: user,
                });

                break;
            }
        }



        helper.goToPage('/user/'+client_id);
    };

    handleStoreChange = () => {

    };


    render(){
        const { column, tickets, direction } = this.state;
        const {userState} = store.getState();
        return (
            <div>
                <Header size='medium'>Список задач  <Button size='mini' color="green" onClick={this.handleAdd}>Добавить</Button></Header>

                <Table sortable celled striped selectable fixed>
                    <Table.Header>
                        <Table.Row>
                            {/*<Table.HeaderCell sorted={column === 'admin_id' ? direction : null} onClick={this.handleSort('admin_id')}>*/}
                                {/*Админ*/}
                            {/*</Table.HeaderCell>*/}
                            <Table.HeaderCell sorted={column === 'client_id' ? direction : null} onClick={this.handleSort('client_id')}>
                                Клиент
                            </Table.HeaderCell>
                            <Table.HeaderCell sorted={column === 'title' ? direction : null} onClick={this.handleSort('title')}>
                                Заголовок
                            </Table.HeaderCell>
                            <Table.HeaderCell sorted={column === 'ticket_status_id' ? direction : null} onClick={this.handleSort('ticket_status_id')}>
                                Статус
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {_.map(tickets, (e, index) => (
                            <Table.Row tabIndex="1" ticket_id={e.id} key={index} onClick={this.handleTicketSelect}>
                                {/*<Table.Cell>*/}
                                    {/*{e.admin == null ? '---' : (*/}
                                        {/*<UserFIO user={e.admin}/>*/}
                                    {/*)}*/}
                                {/*</Table.Cell>*/}
                                <Table.Cell>
                                    {e.client == null ? '---' : (
                                        userState.isAdmin ? (
                                            <Button compact client_id={e.client.id} onClick={this.handleClientSelect}>
                                                <UserFIO user={e.client}/>
                                            </Button>
                                        ) : (
                                            <UserFIO user={e.client}/>
                                        )
                                    )}
                                    </Table.Cell>
                                <Table.Cell>{e.title}</Table.Cell>
                                <Table.Cell>
                                    {e.ticket_status == null ? '---' : (
                                        <Label tag>{e.ticket_status.name}</Label>
                                    )}
                                </Table.Cell>
                            </Table.Row>
                        ))}
                    </Table.Body>
                </Table>

            </div>
        )
    }
};