import React, { Component } from 'react';
import { Button, Image, List, Table, Card, Form, Header, TextArea, Select, Label } from 'semantic-ui-react';

import Diag from '../vendor/Diag';
import store from '../../stores/store';
import helper from '../../services/helper';

const btnStyle = {
    marginLeft: '10px',
};

const labelStyle = {
    marginLeft: '3.5em',
    width: '160%',
    lineHeight: '24px',
    fontSize: '12px',
};

const selectStyle = {
    width: '31%',
    marginLeft: '.5em',
    marginRight: '.5em',
};

export default class TicketCard extends Component{
    state = {
        ticket: null,
        clients: [],
        admins: [],
        ticket_statuses: [],
        diag: {
            open: false,
        }
    };

    componentDidMount = () => {
        const {selectedTicketState, userState} = store.getState();

        if(this.props.isNew){
            this.setState({
                ticket: {
                    title: '',
                    description: '',
                    client_id: '',
                    ticket_status_id: '',
                    ticket_status: {
                        name: 'Новая'
                    }
                }
            });
        }else{
            if(selectedTicketState != null){
                this.setState({
                    ticket: selectedTicketState,
                });
            }else{
                this.getTicket();
            }
        }

        if(userState.isAdmin){
            this.getClientsAndAdmins();
            this.getTicketStatuses();
        }
    };

    getTicketStatuses = () => {
        helper.http_send({
            url: 'admin/tickets',
            method: 'getTicketStatuses',
            onSuccess: (data) => {
                let ticket_statuses = [];
                data.items.forEach(e => {
                    ticket_statuses.push({
                        key: e.id,
                        value: e.id,
                        text: e.name,
                    })
                });
                this.setState({
                    ticket_statuses: ticket_statuses,
                })
            }
        })
    };

    getClientsAndAdmins = () => {
        helper.http_send({
            url: 'admin/users',
            method: 'getClientsAndAdmins',
            onSuccess: (data) => {
                let clients = [];
                let admins = [];
                data.clients.forEach(e => {
                    clients.push({
                        key: e.id,
                        value: e.id,
                        text: e.user_info.name_f+' '+e.user_info.name_i+' '+e.user_info.name_o,
                    })
                });
                data.admins.forEach(e => {
                    admins.push({
                        key: e.id,
                        value: e.id,
                        text: e.email,
                    })
                });
                this.setState({
                    clients: clients,
                    admins: admins,
                })
            }
        })
    };

    getTicket = () => {
        helper.http_send({
            url: 'admin/tickets',
            method: 'getById',
            send: {
                id: this.props.ticketId,
            },
            onSuccess: (data) => {
                this.setState({
                    ticket: data.item,
                })
            }
        })
    };


    handleTicketChange = (e, {value, field_type}) => {

        let ticket = {...this.state.ticket};
        ticket[field_type] = value;

        this.setState({
            ticket: ticket,
        });

    };


    handleSave = () => {
        let {ticket} = this.state;

        this.ticketValidate(() => {
            helper.http_send({
                url: 'admin/tickets',
                method: 'saveTicket',
                send: {
                    ticket: ticket,
                },
                onSuccess: (data) => {
                    let ticket = {...this.state.ticket};

                    this.setState({
                        ticket: ticket,
                    });
                }
            })
        });
    };

    handleDelete = () => {
        this.setState({
            diag: {
                open: true,
                size: 'mini',
                title: 'Удаление задачи',
                text: 'Вы уверены что хотите удалить эту задачу?',
                onClose: () => {
                    this.setState({
                        diag: {
                            open: false,
                        }
                    })
                },
                btns: [
                    {
                        title: 'Удалить',
                        color: 'red',
                        onClick: () => {
                            helper.http_send({
                                url: 'admin/tickets',
                                method: 'delete',
                                send: {
                                    id: this.props.ticketId,
                                },
                                onSuccess: (data) => {
                                    this.state.diag.onClose();
                                    helper.goToPage('tickets');
                                }
                            })
                        }
                    },
                    {
                        title: 'Отмена',
                        color: 'blue',
                        onClick: () => {
                            this.state.diag.onClose();
                        }
                    }
                ]
            }
        })
    };

    handleCreate = () => {
        let {ticket} = this.state;

        this.ticketValidate(() => {
            helper.http_send({
                url: 'admin/tickets',
                method: 'createTicket',
                send: {
                    ticket: ticket,
                },
                onSuccess: (data) => {
                    helper.goToPage('tickets');
                }
            })
        });
    };

    handleCancel = () => {
        helper.goToPage('tickets');
    };

    ticketValidate = (nextFunction = ()=>{}) => {
        let {ticket} = this.state;
        const {userState} = store.getState();

        var s = '';
        if(ticket.title.length<3){
            s = '[Слишком короткий заголовок]';
        }
        if(userState.isAdmin && !ticket.client_id>0){
            if(s!=''){
                s+=', ';
            }
            s += '[Клиент не выбран]';
        }
        if(userState.isAdmin && !ticket.ticket_status_id>0){
            if(s!=''){
                s+=', ';
            }
            s += '[Статус не выбран]';
        }
        if(s == ''){
            nextFunction();
        }else{
            this.setState({
                diag: {
                    open: true,
                    title: 'Ошибка создания',
                    text: s,
                    onClose: () => {
                        this.setState({
                            diag: {
                                open: false,
                            }
                        })
                    },
                    btns: [
                        {
                            title: 'Понятно',
                            color: 'green',
                            onClick: () => {
                                this.state.diag.onClose();
                            }
                        }
                    ]
                }
            });
        }
    };

    render(){
        const {isNew} = this.props;
        const {ticket,clients,admins,ticket_statuses} = this.state;
        const {userState} = store.getState();

        if(ticket!=null && ticket.description==null){
            ticket.description = '';
        }

        return (
            <div>
                <Header size='medium'>Карточка задачи</Header>

                <div>
                    {ticket==null ? (
                        <p>Загрузка...</p>
                    ) : (
                        (userState!=null && userState.isAdmin) ? (
                            <Form>
                                <Form.Group>
                                    <Form.Input field_type="title" placeholder='Заголовок' value={ticket.title} width={6} onChange={this.handleTicketChange} />
                                    <Form.Field field_type="client_id" control={Select} placeholder='Клиент' options={clients} value={ticket.client_id} width={6} onChange={this.handleTicketChange} />
                                    {/*<Form.Field field_type="admin_id" control={Select} placeholder='Админ' options={clients} value={ticket.client_id} width={6} onChange={this.handleTicketChange} />*/}
                                    <Form.Field field_type="ticket_status_id" control={Select} placeholder='Статус' options={ticket_statuses} value={ticket.ticket_status_id} width={6} onChange={this.handleTicketChange} />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Field field_type="description" control={TextArea} placeholder='Описание' value={ticket.description} width={16} onChange={this.handleTicketChange}/>
                                </Form.Group>
                            </Form>
                        ) : ( isNew ? (
                                <Form>
                                    <Form.Group>
                                        <Form.Input field_type="title" placeholder='Заголовок' value={ticket.title} width={6} onChange={this.handleTicketChange} />
                                        <Label style={labelStyle} tag>{ticket.ticket_status.name}</Label>
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Field field_type="description" control={TextArea} placeholder='Описание' value={ticket.description} width={16} onChange={this.handleTicketChange}/>
                                    </Form.Group>
                                </Form>
                            ) : (
                                <Form>
                                    <Form.Group>
                                        <Label style={labelStyle}>{ticket.title}</Label>
                                        <Label style={labelStyle} tag>{ticket.ticket_status.name}</Label>
                                        <Label style={labelStyle}>{'Админ - ['+(ticket.admin==null?'---':ticket.admin.email)+']'}</Label>
                                    </Form.Group>
                                    <Form.Group>
                                        <Label style={labelStyle}>
                                            <p>{ticket.description}</p>
                                        </Label>
                                    </Form.Group>
                                </Form>
                            )
                        )
                    )}
                </div>

                <div>
                    {isNew ? (
                        <span>
                                  <Button basic color='green' onClick={this.handleCreate}>Создать</Button>
                                  <Button style={btnStyle} basic color='blue' onClick={this.handleCancel}>Отмена</Button>
                              </span>
                    ) : (
                        <span>
                            {userState==null || userState.isAdmin==false || (
                               <span>
                                   <Button basic color='green' onClick={this.handleSave}>Сохранить</Button>
                                   <Button style={btnStyle} basic color='red' onClick={this.handleDelete}>Удалить</Button>
                               </span>
                            ) }
                                    <Button style={btnStyle} basic color='blue' onClick={this.handleCancel}>Отмена</Button>
                                </span>
                    )}
                </div>

                <Diag model={this.state.diag} />
            </div>
        )
    }
}