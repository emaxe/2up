import React, { Component } from 'react';

import TicketCard from './TicketCard';
import store from '../../stores/store';
import helper from '../../services/helper';

export default class TicketAdd extends Component{

    componentDidMount = () => {
        store.dispatch({
            type: 'SET_PAGE_NAME',
            pageName: 'tickets',
        });
    };

    render(){

        return (
            <TicketCard isNew={true}/>
        )
    }
}