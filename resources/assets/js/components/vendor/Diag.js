import React, { Component } from 'react'
import { Button, Modal } from 'semantic-ui-react'

class Diag extends Component {

    render() {
        const { open = false, size = 'mini', title = '', text = '', btns = [], onClose =()=>{} } = this.props.model;
        return (
            <div>
                <Modal basic size={size} open={open} onClose={onClose}>
                    <Modal.Header>
                        {title}
                    </Modal.Header>
                    <Modal.Content>
                        <p>{text}</p>
                    </Modal.Content>
                    <Modal.Actions>
                        {_.map(btns, (e, index) => (
                            <Button color={e.color} key={index} onClick={e.onClick}>
                                {e.title}
                            </Button>
                        ))}
                    </Modal.Actions>
                </Modal>
            </div>
        )
    }
}

export default Diag
