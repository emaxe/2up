import React, { Component } from 'react';
import { Header, Label } from 'semantic-ui-react'

export default class ScreenTitle extends Component{

    spanStyle = {
        marginLeft: '10px',
    };

    render(){
        const {user} = this.props;
        return (
            <Header as='h4' color='teal'>Tickets system  <span style={this.spanStyle}>{user==null || ( <Label as='a' color='teal' tag>{user.isAdmin?'админ':'клиент'}</Label>)}</span></Header>
        )
    }
}