import React, { Component } from 'react';
import { Menu, Segment, Header, Icon, Label, Dropdown, Container } from 'semantic-ui-react';
import { Switch, Route } from 'react-router-dom';
import Loadable from 'react-loadable';

import ScreenTitle from './vendor/ScreenTitle';
import Diag from './vendor/Diag';

import store from '../stores/store';
import helper from '../services/helper';




function MyLoadingComponent() {
    return <div>Загрузка...</div>;
};

const Home = Loadable({
    loader: () => import('../_components/Home'),
    loading: MyLoadingComponent
});

const Tickets = Loadable({
    loader: () => import('./tickets/Tickets'),
    loading: MyLoadingComponent
});

const TicketEdit = Loadable({
    loader: () => import('./tickets/TicketEdit'),
    loading: MyLoadingComponent
});

const TicketAdd = Loadable({
    loader: () => import('./tickets/TicketAdd'),
    loading: MyLoadingComponent
});

const Users = Loadable({
    loader: () => import('./users/Users'),
    loading: MyLoadingComponent
});

const UserEdit = Loadable({
    loader: () => import('./users/UserEdit'),
    loading: MyLoadingComponent
});

const UserAdd = Loadable({
    loader: () => import('./users/UserAdd'),
    loading: MyLoadingComponent
});

// const Logout = Loadable({
//     loader: () => import('./Logout'),
//     loading: MyLoadingComponent
// });


class Screen extends Component{
    state = {
        user: null,
        pageName: store.getState().pageNameState,
        diag: {
            open: false,
        },
        menuItems: [],
    };

    componentDidMount = () => {
        this.whoAmI();
        store.subscribe(this.handleStoreChange);
    };

    whoAmI = () => {
        helper.http_send({
            url: 'admin/users',
            method: 'whoAmI',
            onSuccess: (data) => {
                store.dispatch({
                    type: 'SET_USER',
                    user: data.item,
                    isAdmin: data.isAdmin,
                });

                let menuItems = [];

                if(data.isAdmin){
                    menuItems = [
                        {
                            page_name: 'tickets',
                            name: 'Задачи',
                            alias: 'tickets',
                        },
                        {
                            page_name: 'users',
                            name: 'Клиенты',
                            alias: 'users',
                        }
                    ]
                }else{
                    menuItems = [
                        {
                            page_name: 'tickets',
                            name: 'Задачи',
                            alias: 'tickets',
                        }
                    ]
                }

                this.setState({
                    user: store.getState().userState,
                    menuItems: menuItems,
                });
            }
        });
    };



    handleStoreChange = () => {
        this.setState({
            pageName: store.getState().pageNameState,
        })
    };

    handleItemClick = (e, { page_name }) => {
        helper.goToPage(page_name);
    };

    handleExit = () => {
        this.setState({
            diag: {
                open: true,
                title: 'Выход из приложения',
                text: 'Вы уверены что хотите выйти?',
                onClose: () => {
                    this.setState({
                        diag: {
                            open: false,
                        }
                    })
                },
                btns: [
                    {
                        title: 'Выйти',
                        color: 'red',
                        onClick: () => {
                            document.location.href = '/logout';
                            this.state.diag.onClose();
                        }
                    },
                    {
                        title: 'Отмена',
                        color: 'blue',
                        onClick: () => {
                            this.state.diag.onClose();
                        }
                    }
                ]
            }
        });
    };

    render(){
        const { user, menuItems } = this.state;

        return (
            <Container fluid>

                <Menu pointing secondary>
                    <Menu.Item header>
                        <ScreenTitle user={this.state.user} />
                    </Menu.Item>
                    <Menu.Item page_name="/" name='Главная' active={this.state.pageName === 'home'} onClick={this.handleItemClick} />

                    {_.map(menuItems, (e, index) => (
                        <Menu.Item key={index} page_name={e.page_name} name={e.name} active={this.state.pageName === e.alias} onClick={this.handleItemClick} />
                    ))}

                    {/*<Menu.Item page_name="tickets" name='Задачи' active={this.state.pageName === 'tickets'} onClick={this.handleItemClick} />*/}
                    {/*<Menu.Item page_name="users" name='Пользователи' active={this.state.pageName === 'users'} onClick={this.handleItemClick} />*/}


                    <Menu.Menu position='right'>
                        <Menu.Item page_name="logout" name='Выход' active={this.state.pageName === 'logout'} onClick={this.handleExit} />
                    </Menu.Menu>
                </Menu>

                <Segment>

                    <Switch>
                        <Route exact path='/' component={Home}/>
                        <Route path='/tickets' component={Tickets}/>
                        <Route path='/ticket/:id' component={TicketEdit}/>
                        <Route path='/ticket_add' component={TicketAdd}/>
                        <Route path='/users' component={Users}/>
                        <Route path='/user/:id' component={UserEdit}/>
                        <Route path='/user_add' component={UserAdd}/>
                        {/*<Route path='/logout/' component={Logout}/>*/}
                    </Switch>

                </Segment>
                <Diag model={this.state.diag} />
            </Container>
        )
    }
};

export default Screen;