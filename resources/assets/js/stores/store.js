import { createStore, combineReducers } from 'redux';

const userReducer = function(state, action) {
    if (state === undefined) {
        state = null;
    }
    if (action.type === 'SET_USER') {
        state = action.user;
        state.isAdmin = action.isAdmin;
    }
    return state;
};

const selectedUserReducer = function(state, action) {
    if (state === undefined) {
        state = null;
    }
    if (action.type === 'SET_SELECTED_USER') {
        state = action.user;
    }
    return state;
};

const selectedTicketReducer = function(state, action) {
    if (state === undefined) {
        state = null;
    }
    if (action.type === 'SET_SELECTED_TICKET') {
        state = action.ticket;
    }
    return state;
};

const tiketsReducer = function(state, action) {
    if (state === undefined) {
        state = [];
    }
    if (action.type === 'SET_TICKETS') {
        state = action.tickets;
    }
    if (action.type === 'ADD_TICKET') {
        state.push(action.ticket);
    }
    return state;
};

const pageNameReducer = function(state, action) {
    if (state === undefined) {
        state = null;
    }
    if (action.type === 'SET_PAGE_NAME') {
        state = action.pageName;
    }
    return state;
};

const reducers = combineReducers({
    userState: userReducer,
    selectedUserState: selectedUserReducer,
    selectedTicketState: selectedTicketReducer,
    ticketsState: tiketsReducer,
    pageNameState: pageNameReducer,
});

const store = createStore(reducers);

export default store;