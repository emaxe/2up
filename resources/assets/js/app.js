import React from 'react';
import ReactDom from 'react-dom';
import { render } from 'react-dom';
import { HashRouter } from 'react-router-dom';

import Screen from './components/Screen';

import './bootstrap';
import 'typeface-roboto';
import 'semantic-ui-css/semantic.min.css';
import 'semantic-ui/dist/semantic.css';

ReactDom.render(
    <HashRouter>
        <Screen />
    </HashRouter>
    ,
    document.getElementById('app')
);