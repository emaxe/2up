export default{
    fLoad: [],
    goToPage(page){
        document.location.hash = page;
    },
    http_send(o){
        var f = true;
        var msgs = [];

        if(o.url == undefined){
            f = false;
            msgs.push('URL is undefined');
        }

        if(o.method == undefined){
            f = false;
            msgs.push('METHOD is undefined')
        }

        if(o.send == undefined){
            o.send = {};
        }

        if(f){
            this.fLoad[o.f] = true;
            o.send.method = o.method;
            axios.post(''+o.url, o.send).then(function(response){
                console.log('>> HTTP_SEND -> SUCCESS ->',o.url, o.method, response);
                var data = response.data;

                if(data.state == 0){

                    if(typeof (o.onSuccess) === 'function'){
                        o.onSuccess(data);
                    }

                }else{

                    if(typeof (o.onStateError) === 'function'){
                        o.onStateError(data);
                    }else{
                        console.error('>> HTTP_SEND -> ERROR ->','('+o.url+' '+o.method+') Ошибка получения данных ['+data.state+']');
                        // this.showToast({
                        //     text: '('+o.url+' '+o.method+') Ошибка получения данных ['+data.state+']',
                        //     type: 'error',
                        // });
                    }

                }
                this.fLoad[o.f]  = false;
            }.bind(this)).catch(function(error){
                console.error('>> HTTP_SEND -> ERROR ->',o.url, o.url, error);

                if(typeof (o.onHttpError) === 'function'){
                    o.onHttpError(data);
                }

                // this.showToast({
                //     text: '('+o.url+' '+o.url+') Ошибка связи с сервером [ ' + error+' ]',
                //     type: 'error',
                // });
                this.fLoad[o.f]  = false;
            }.bind(this));
        }else{
            console.error('> HTTP_SEND PARAMS ERROR ->', msgs)
        }
    }
}