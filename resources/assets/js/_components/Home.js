import React from 'react';
import { Container, Header } from 'semantic-ui-react'

import helper from '../services/helper';
import store from '../stores/store';

class Home extends React.Component{

    componentDidMount = () => {
        store.dispatch({
            type: 'SET_PAGE_NAME',
            pageName: 'home',
        });
    };

    render(){
        return (
            <Container fluid>
                <Header as='h2'>Вы вошли в систему поддержки пользователей</Header>
            </Container>
        )
    }
}


export default Home;