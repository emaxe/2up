import React from 'react';
import helper from '../services/helper';
import store from '../stores/store';
import User from './User';

class UserEdit extends React.Component{
    constructor(props) {
        super(props);
        this.state = {

        };

        store.dispatch({
            type: 'SET_PAGE_NAME',
            pageName: 'users',
        });

        helper.http_send({
            url: 'admin/users',
            method: 'getById',
            send: {
                id: props.match.params.id,
            },
            onSuccess: (data) => {
                data.item.password1 = data.item.password2 = '';
                this.setState({
                    user: data.item,
                })

                // console.info('> ', this.state.user);
            }
        })
    }

    render(){
        return (
            <div>
                {this.state.user!=null ? (
                    <User isNew={false} user={this.state.user}/>
                ) : (
                    <p>Загрузка...</p>
                ) }
            </div>
        )
    }
}

export default UserEdit;