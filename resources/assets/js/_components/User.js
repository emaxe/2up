import React from 'react';
import helper from '../services/helper';
import store from '../stores/store';
import TextField from 'material-ui/TextField';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Input, { InputLabel } from 'material-ui/Input';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Button from 'material-ui/Button';
import ConfirmDialogComponent from './ConfirmDialogComponent';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        width: '100%',
        margin: theme.spacing.unit,
    },
    button: {
        margin: theme.spacing.unit,
    },
});

class User extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            isNew: this.props.isNew,
            user: this.props.user,
            confirmDialog: {
                isShow: false,
                btns: []
            }
        };

        // store.dispatch({
        //     type: 'SET_PAGE_NAME',
        //     pageName: 'users',
        // });
        //
        // helper.http_send({
        //     url: 'admin/users',
        //     method: 'getById',
        //     send: {
        //         id: props.match.params.id,
        //     },
        //     onSuccess: (data) => {
        //         data.item.password1 = data.item.password2 = '';
        //         this.setState({
        //             user: data.item,
        //         })
        //
        //         console.info('> ', this.state.user);
        //     }
        // })
    }

    handleSave = () => {
        var s = '';
        if(this.state.user.password1.length > 0){
            if(this.state.user.password1 != this.state.user.password2){
                s = '[Пароли не совпадают]';
            }
            if(this.state.user.password1.length<3){
                if(s!=''){
                    s +=', ';
                }
                s += ']Длина пароля не может быть меньше 3 символов]';
            }
        }
        if(s == ''){
            helper.http_send({
                url: 'admin/users',
                method: 'save',
                send: {
                    user: this.state.user,
                },
                onSuccess: (data) => {
                    let user = {...this.state.user};
                    user.password1 = user.password2 = '';

                    this.setState({
                        user: user,
                    });
                }
            })
        }else{
            this.setState({
                confirmDialog: {
                    isShow: true,
                    title: 'Ошибка сохранения',
                    text: s,
                    btns: [
                        {
                            title: 'Понятно',
                            color: 'primary',
                            onClick: () => {
                                this.setState({
                                    confirmDialog: {
                                        isShow: false,
                                    },
                                });
                            }
                        }
                    ]
                }
            });
        }

    };

    handleDelete = () => {
        this.setState({
            confirmDialog: {
                isShow: true,
                title: 'Удаление пользователя',
                text: 'Вы уверены что хотите удалить пользователя?',
                btns: [
                    {
                        title: 'Удалить',
                        color: 'accent',
                        onClick: () => {
                            this.setState({
                                confirmDialog: {
                                    text: 'Выполнение запроса...',
                                    btns: [],
                                },
                            });

                            helper.http_send({
                                url: 'admin/users',
                                method: 'delete',
                                send: {
                                    id: this.state.user.id,
                                },
                                onSuccess: (data) => {
                                    this.setState({
                                        confirmDialog: {
                                            isShow: false,
                                        },
                                    });
                                    helper.goToPage('users');
                                }
                            })
                        }
                    },
                    {
                        title: 'Отмена',
                        color: 'primary',
                        onClick: () => {
                            this.setState({
                                confirmDialog: {
                                    isShow: false,
                                },
                            });
                        }
                    }
                ]
            }
        });
    };

    handleUserChange = (e,name) => {

        let user = {...this.state.user};
        user[name] = e.target.value;

        this.setState({
            user: user,
        });

        console.info('> ', this.state.user);
    };

    handleUserInfoChange = (e,name) => {

        let user = {...this.state.user};
        user.user_info[name] = e.target.value;

        this.setState({
            user: user,
        });

        console.info('> ', this.state.user.user_info);
    };

    render(){
        const { classes } = this.props;
        return (
            <div>
                <h3>Карточка клиента:</h3>
                <div className={classes.container}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="company">Компания</InputLabel>
                        <Input id="company" value={this.state.user.user_info.company_name} onChange={e => this.handleUserInfoChange(e,'company_name')} />
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="name-f">Фамилия</InputLabel>
                        <Input id="name-f" value={this.state.user.user_info.name_f} onChange={e => this.handleUserInfoChange(e,'name_f')} />
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="name-i">Имя</InputLabel>
                        <Input id="name-i" value={this.state.user.user_info.name_i} onChange={e => this.handleUserInfoChange(e,'name_i')} />
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="name-o">Отчество</InputLabel>
                        <Input id="name-o" value={this.state.user.user_info.name_o} onChange={e => this.handleUserInfoChange(e,'name_o')} />
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="phone">Телефон</InputLabel>
                        <Input id="phone" value={this.state.user.user_info.phone} onChange={e => this.handleUserInfoChange(e,'phone')} />
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="site_url">Сайт</InputLabel>
                        <Input id="site_url" value={this.state.user.user_info.site_url} onChange={e => this.handleUserInfoChange(e,'site_url')} />
                    </FormControl>
                    <hr/>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="email">Email</InputLabel>
                        <Input id="email" value={this.state.user.email} onChange={e => this.handleUserChange(e,'email')} />
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="password1">Новый пароль</InputLabel>
                        <Input type="password" id="password1" value={this.state.user.password1} onChange={e => this.handleUserChange(e,'password1')} />
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="password2">Новый пароль еще раз</InputLabel>
                        <Input type="password" id="password2" value={this.state.user.password2} onChange={e => this.handleUserChange(e,'password2')} />
                    </FormControl>
                    <hr/>
                    <div>
                        <Button raised color="primary" className={classes.button} onClick={this.handleSave}>
                            Сохранить
                        </Button>
                        <Button raised color="accent" className={classes.button} onClick={this.handleDelete}>
                            Удалить пользователя
                        </Button>
                    </div>
                </div>
                <ConfirmDialogComponent model={this.state.confirmDialog} />
            </div>
        )
    }
}

User.propTypes = {
    classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(User);