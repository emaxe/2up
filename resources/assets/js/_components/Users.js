import React from 'react';
import helper from '../services/helper';
import store from '../stores/store';
import UserList from './UserList';

class Users extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            users: null,
        };

        store.dispatch({
            type: 'SET_PAGE_NAME',
            pageName: 'users',
        });

        helper.http_send({
            f: 'g',
            url: 'admin/users',
            method: 'getClients',
            onSuccess: (data) => {
                this.setState({
                    users: data.items,
                })

                console.info('> ', this.state.users);
            }
        })
    }

    render(){
        return (
            <div>
                <h3>Список клиентов:</h3>
                {this.state.users==null?'Загрузка..':(this.state.users.length>0 ? <UserList model={this.state.users} /> : 'Ничего нет...')}
            </div>
        )
    }
}


export default Users;