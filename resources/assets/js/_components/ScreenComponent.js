import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import classNames from 'classnames';
import Drawer from 'material-ui/Drawer';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import ChevronLeftIcon from 'material-ui-icons/ChevronLeft';
import InboxIcon from 'material-ui-icons/Inbox';
import ChevronRightIcon from 'material-ui-icons/ChevronRight';
import PeopleIcon from 'material-ui-icons/People';
import ExitToAppIcon from 'material-ui-icons/ExitToApp';
import ConfirmDialogComponent from './ConfirmDialogComponent';
import { Switch, Route } from 'react-router-dom'
import Loadable from 'react-loadable';
import helper from '../services/helper';
import store from '../stores/store';
import { connect } from 'react-redux'

function MyLoadingComponent() {
    return <div>Загрузка...</div>;
}

const Home = Loadable({
    loader: () => import('./Home'),
    loading: MyLoadingComponent
});

const Users = Loadable({
    loader: () => import('./Users'),
    loading: MyLoadingComponent
});

const UserEdit = Loadable({
    loader: () => import('./UserEdit'),
    loading: MyLoadingComponent
});


const drawerWidth = 240;

const activeStyle = {
    backgroundColor: 'rgba(0, 0, 0, 0.12)',
};
const blankStyle = {};

const styles = theme => ({
    root: {
        width: '100%',
        height: '100%',
        // marginTop: theme.spacing.unit * 3,
        zIndex: 1,
        overflow: 'hidden',
    },
    appFrame: {
        position: 'relative',
        display: 'flex',
        width: '100%',
        height: '100%',
    },
    appBar: {
        position: 'absolute',
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    drawerPaper: {
        position: 'relative',
        height: '100%',
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    content: {
        overflowY: 'scroll',
        width: '100%',
        marginLeft: -drawerWidth,
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        height: 'calc(100% - 56px)',
        marginTop: 56,
        [theme.breakpoints.up('sm')]: {
            content: {
                height: 'calc(100% - 64px)',
                marginTop: 64,
            },
        },
    },
    contentShift: {
        marginLeft: 0,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
});

class ScreenComponent extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            pageName: store.getState().pageNameState,
            open: true,
            user: null,
            confirmDialog: {
                isShow: false,
                btns: []
            }
        };
        helper.http_send({
            f: 'g',
            url: 'admin/users',
            method: 'whoAmI',
            onSuccess: (data) => {
                store.dispatch({
                    type: 'SET_USER',
                    user: data.item,
                    isAdmin: data.isAdmin,
                });

                this.setState({
                    user: store.getState().userState,
                });

                console.info('>>', store.getState());
            }
        });

        store.subscribe(this.handleStoreChange);
    }

    goToPage(page){
        document.location.hash = page;
    };

    exit = () =>{
        this.setState({
            confirmDialog: {
                isShow: true,
                title: 'Выход из приложения',
                text: 'Вы уверены что хотите выйти?',
                btns: [
                    {
                        title: 'Выход',
                        color: 'accent',
                        onClick: () => {
                            this.setState({
                                confirmDialog: {
                                    isShow: false,
                                },
                            });
                            document.location.href = '/logout';
                        }
                    },
                    {
                        title: 'Отмена',
                        color: 'primary',
                        onClick: () => {
                            this.setState({
                                confirmDialog: {
                                    isShow: false,
                                },
                            });
                        }
                    }
                ]
            }
        });
    };

    handleStoreChange = () => {
        this.setState({
            pageName: store.getState().pageNameState,
        })
    };

    handleDrawerOpen = () => {
        this.setState({ open: true });
    };

    handleDrawerClose = () => {
        this.setState({ open: false });
    };

    render() {
        const { classes, theme } = this.props;
        return (
            <div className={classes.root}>
                <div className={classes.appFrame}>
                    <AppBar className={classNames(classes.appBar, this.state.open && classes.appBarShift)}>
                        <Toolbar disableGutters={!this.state.open}>
                            <IconButton
                                color="contrast"
                                aria-label="open drawer"
                                onClick={this.handleDrawerOpen}
                                className={classNames(classes.menuButton, this.state.open && classes.hide)}
                            >
                                <MenuIcon />
                            </IconButton>
                            <Typography type="headline" color="inherit" noWrap>
                                Ticket system {this.state.user==null || ' - (Вы вошли как ' + (this.state.user.isAdmin?' администратор':'клиент') + ')'}
                            </Typography>
                        </Toolbar>
                    </AppBar>
                    <Drawer
                        type="persistent"
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        open={this.state.open}
                    >
                        <div className={classes.drawerInner}>
                            <div className={classes.drawerHeader}>
                                <IconButton onClick={this.handleDrawerClose}>
                                    {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                                </IconButton>
                            </div>
                            <Divider />
                            {this.state.user==null || ((this.state.user.isAdmin)?( //Admin menu items
                                <List className={classes.list}>
                                    <ListItem button onClick={() => this.goToPage('/')} style={this.state.pageName == 'home' ? activeStyle : blankStyle}>
                                        <ListItemIcon>
                                            <InboxIcon />
                                        </ListItemIcon>
                                        <ListItemText disableTypography={true} primary="Задачи" />
                                    </ListItem>
                                    <ListItem button onClick={() => this.goToPage('/users')} style={this.state.pageName == 'users' ? activeStyle : blankStyle}>
                                        <ListItemIcon>
                                            <PeopleIcon />
                                        </ListItemIcon>
                                        <ListItemText disableTypography={true} primary="Клиенты" />
                                    </ListItem>
                                </List>
                            ):(// Client menu items
                                <List className={classes.list}>
                                    <ListItem button onClick={() => this.goToPage('/')}>
                                        <ListItemIcon>
                                            <InboxIcon />
                                        </ListItemIcon>
                                        <ListItemText disableTypography={true} primary="Задачи" />
                                    </ListItem>
                                </List>
                            ))}
                            <Divider />
                            <List className={classes.list}>
                                <ListItem button onClick={this.exit}>
                                    <ListItemIcon>
                                        <ExitToAppIcon />
                                    </ListItemIcon>
                                    <ListItemText disableTypography={true} primary="Выход" />
                                </ListItem>
                            </List>
                        </div>
                    </Drawer>
                    <main className={classNames(classes.content, this.state.open && classes.contentShift)}>
                        <Switch>
                            <Route exact path='/' component={Home}/>
                            <Route path='/users' component={Users}/>
                            <Route path='/user/:id' component={UserEdit}/>
                        </Switch>
                    </main>
                </div>
                <ConfirmDialogComponent model={this.state.confirmDialog} />
            </div>
        );
    }
};

ScreenComponent.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};


export default withStyles(styles, { withTheme: true })(ScreenComponent);