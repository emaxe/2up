import React from 'react';
import Button from 'material-ui/Button';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog';
import Slide from 'material-ui/transitions/Slide';

export default class ConfirmDialogComponent extends React.Component {
    render() {

        return (
            <div>
                <Dialog
                    open={this.props.model.isShow}
                    transition={<Slide direction="up" />}
                    keepMounted
                    onRequestClose={this.handleRequestClose}
                >
                    <DialogTitle>{this.props.model.title}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            {this.props.model.text}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        {this.props.model.btns==undefined || this.props.model.btns.map((e, index) =>
                            <Button onClick={e.onClick} color={e.color==undefined?'default':e.color} key={index}>
                                {e.title}
                            </Button>
                        )}
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}