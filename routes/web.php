<?php

Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('register', function () {
    return view('admin.index');
});

Route::get('/', function () {
    return view('admin.index');
})->middleware('auth');


$admin_router = function($pr){
    Route::post('/'.$pr.'/users', 'UserController@index');
    Route::post('/'.$pr.'/tickets', 'TicketController@index');
    Route::post('/'.$pr.'/comments', 'CommentController@index');
};

Route::group([
    'namespace' => 'Admin',
    'middleware' => [
        'auth',
    ]
], function() use($admin_router) {
    $admin_router('admin');
});
