<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_infos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->nullable()->unsigned();
            $table->string('company_name')->nullable();
            $table->string('name_f')->nullable();
            $table->string('name_i')->nullable();
            $table->string('name_o')->nullable();
            $table->string('site_url')->nullable();
            $table->string('phone')->nullable();
            $table->string('description')->nullable();
            $table->dateTime('last_login')->nullable();

            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_infos');
    }
}
