<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ticket_statuses')->insert([
            [
                'id' => 1,
                'name' => 'новая',
            ],
            [
                'id' => 2,
                'name' => 'просмотрена',
            ],
            [
                'id' => 3,
                'name' => 'оплачена',
            ],
            [
                'id' => 4,
                'name' => 'выполнена',
            ],
            [
                'id' => 5,
                'name' => 'закрыта',
            ],
        ]);

        DB::table('tickets')->insert([
            [
                'admin_id' => null,
                'client_id' => 2,
                'ticket_status_id' => 1,
                'title' => 'Задача 1',
                'description' => 'Описание задачи',
            ],
            [
                'admin_id' => null,
                'client_id' => 3,
                'ticket_status_id' => 1,
                'title' => 'Задача 2',
                'description' => 'Описание задачи 2',
            ],
        ]);
    }
}
