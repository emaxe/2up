<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id' => 1,
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('password'),
            ],
            [
                'id' => 2,
                'name' => 'client',
                'email' => 'client@gmail.com',
                'password' => bcrypt('password'),
            ],
            [
                'id' => 3,
                'name' => 'client2',
                'email' => 'client2@gmail.com',
                'password' => bcrypt('password'),
            ],
            [
                'id' => 4,
                'name' => 'client3',
                'email' => 'client3@gmail.com',
                'password' => bcrypt('password'),
            ]
        ]);

        DB::table('user_infos')->insert([
            [
                'user_id' => 2,
                'company_name' => 'Company 1',
                'name_f' => 'Иванов',
                'name_i' => 'Иван',
                'name_o' => 'Иванович',
                'site_url' => 'http://site.com',
                'phone' => '+79876541230',
                'description' => '-',
            ],
            [
                'user_id' => 3,
                'company_name' => 'Company 2',
                'name_f' => 'Петров',
                'name_i' => 'Петр',
                'name_o' => 'Петрович',
                'site_url' => 'http://site2.com',
                'phone' => '+79546541230',
                'description' => '-',
            ],
            [
                'user_id' => 4,
                'company_name' => 'Company 3',
                'name_f' => 'Сидоров',
                'name_i' => 'Сидор',
                'name_o' => 'Сидорович',
                'site_url' => 'http://site3.com',
                'phone' => '+79546544530',
                'description' => '-',
            ],
        ]);

        DB::table('roles')->insert([
            [
                'id' => 1,
                'name' => 'admin',
            ],
            [
                'id' => 2,
                'name' => 'client',
            ]
        ]);

        DB::table('role_user')->insert([
            [
                'user_id' => 1,
                'role_id' => 1,
            ],
            [
                'user_id' => 2,
                'role_id' => 2,
            ],
            [
                'user_id' => 3,
                'role_id' => 2,
            ],
            [
                'user_id' => 4,
                'role_id' => 2,
            ],
        ]);
    }
}
